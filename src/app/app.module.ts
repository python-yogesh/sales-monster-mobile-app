import { FormsModule } from '@angular/forms';
import { MbscModule, mobiscroll } from '@mobiscroll/angular-trial';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
// import components
import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ConfirmRegistrationPage } from '../pages/confirm-registration/confirm-registration';
import { CompanyRegistrationPage } from '../pages/company-registration/company-registration';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { ProfilePage } from '../pages/profile/profile';
import { ContactPage } from '../pages/contact/contact';
import { AddDealPage } from '../pages/add-deal/add-deal';
import { SearchPage } from '../pages/search/search';
import { AddPersonPage } from '../pages/add-person/add-person';
import { DealDetailPage } from '../pages/deal-detail/deal-detail';
import { TabsPage } from '../pages/tabs/tabs';
import { AddActivityPage } from '../pages/add-activity/add-activity';
import { EventsCalendarPage } from '../pages/events-calendar/events-calendar';
import { LostReasonPage } from '../pages/lost-reason/lost-reason';
import { PersonDetailPage } from '../pages/person-detail/person-detail';
import { ForgetPasswordPage } from '../pages/forget-password/forget-password';
import { OrganisationDetailPage } from '../pages/organisation-detail/organisation-detail';
import { CustomFieldPage } from '../pages/custom-field/custom-field';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { StatisticsPage } from '../pages/statistics/statistics';
import { ProductDetailPage } from '../pages/product-detail/product-detail';
import { AddProductPage } from '../pages/add-product/add-product';

// import services
import { UserLoginService } from "../providers/userLogin.service";
import { UserParametersService } from "../providers/userParameters.service";
import { UserRegistrationService } from "../providers/userRegistration.service";
import { CognitoUtil } from "../providers/cognito.service";
import { AwsUtil } from "../providers/aws.service";
import { EventsService } from "../providers/events.service";
import { UserService, CompanyService, PipelineService, DealService, ContactService, AddActivityService, StripeServices } from "../providers/api-services/user.service";

mobiscroll.apiKey = '7bc89203';

@NgModule({
  declarations: [
    MyApp, LoginPage, SignupPage, ConfirmRegistrationPage, CompanyRegistrationPage, DashboardPage, ProfilePage, ContactPage, 
    AddDealPage, SearchPage, TabsPage, AddPersonPage, DealDetailPage, AddActivityPage, EventsCalendarPage, LostReasonPage,PersonDetailPage,OrganisationDetailPage,CustomFieldPage, ForgetPasswordPage,ChangePasswordPage,StatisticsPage,ProductDetailPage,AddProductPage,
  ],
  imports: [ 
    FormsModule, 
    MbscModule,
    BrowserModule, HttpModule, IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp, LoginPage, SignupPage, ConfirmRegistrationPage, CompanyRegistrationPage, DashboardPage, ProfilePage, ContactPage, 
    AddDealPage, SearchPage, TabsPage, AddPersonPage, DealDetailPage, AddActivityPage, EventsCalendarPage, LostReasonPage,PersonDetailPage,OrganisationDetailPage,CustomFieldPage, ForgetPasswordPage,ChangePasswordPage,StatisticsPage,ProductDetailPage,AddProductPage,
  ],
  providers: [
    StatusBar, SplashScreen, 
    CognitoUtil, AwsUtil, UserLoginService, UserParametersService, UserRegistrationService, EventsService, UserService, CompanyService, PipelineService, DealService, ContactService,AddActivityService, StripeServices,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}