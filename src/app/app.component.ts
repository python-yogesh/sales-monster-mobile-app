import { Component } from '@angular/core';
import { Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import component
import { LoginPage } from '../pages/login/login';
// import { EventsCalendarPage } from '../pages/events-calendar/events-calendar';
import { TabsPage } from '../pages/tabs/tabs';
import { ProfilePage } from '../pages/profile/profile';
// import services
import { AwsUtil } from "../providers/aws.service";
import { UserService, StripeServices } from "../providers/api-services/user.service";
import { UserLoginService } from "../providers/userLogin.service";

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  public rootPage:any;
  constructor(public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, 
              public awsUtil: AwsUtil, public event: Events, public auth_user_services: UserService,
              public userService: UserLoginService, public stripe_services: StripeServices ) {
    platform.ready().then(() => {
      this.awsUtil.initAwsService();
      // statusBar.styleLightContent();
      splashScreen.hide();
      // let status bar overlay webview
      statusBar.overlaysWebView(true);
      // set status bar to white
      statusBar.backgroundColorByHexString('#ffffff');
      statusBar.styleLightContent();
      statusBar.styleBlackTranslucent();
      statusBar.styleBlackOpaque();
    });
    this.userService.isAuthenticated(this);
    /*console.log('App component');*/
    // alert('load app root');
    
  }

  get_organization(){
    this.auth_user_services.get_organization().subscribe(data=>{
      debugger
      this.get_billing_info(data.response_organizations.Item.uuid);
    },(err) => alert(err));
  }

  get_billing_info(uuid){
    this.stripe_services.get_billing_info_api(uuid).subscribe(data=>{
      console.log(data)
      if(data.status == 'past_due'){
        this.rootPage = ProfilePage; 
      }
    });
  }


  ionViewWillEnter(): void {
    this.platform.registerBackButtonAction(() => this.backButtonFunc());
  }

  private backButtonFunc(): void {
    // alert();
  }

  isLoggedInCallback(message: string, isLoggedIn: boolean) {
    if (isLoggedIn) {
      this.rootPage = TabsPage;
      this.get_organization();
    }else{
      this.rootPage = LoginPage;
    }
  }
}