import { Injectable } from '@angular/core';
import { Headers, Http} from '@angular/http'
import { BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

// import services
import { CognitoUtil } from '../cognito.service';
import { Baseurl } from './baseUrl';

@Injectable()
export class UserService {
  baseUrl: any;
  public NewSub:BehaviorSubject<any> = new BehaviorSubject<any>('');

  constructor(public cogitoUtil: CognitoUtil, public _http: Http){
    var url = new Baseurl()
    this.baseUrl = url.get_url();
    
  }

  /*newUrl = 'https://zemkxckfml.execute-api.us-west-2.amazonaws.com/dev/';*/
  newUrl = 'https://abmrwnvmjb.execute-api.us-east-1.amazonaws.com/beta/';

  header_token(){
    var token = this.cogitoUtil.get_session_Token();
    // console.log(token);
    // var token = localStorage.getItem("aws-IdToken");
    let headers = new Headers();
    headers.append('Authorization', token);
    headers.append('Content-type', 'application/json');
    return headers;
  }

  header_token_(){
    let headers = new Headers();
    headers.append('Content-type', 'application/json');
    return headers;
  }
  
  // create admin on cognito
  add_user_group_(email){
    return this._http.post(this.baseUrl+'v1/add-user-group/'+email,'',{
      headers: this.header_token()
    }).map((res:any)=> res.json());
  }
  // get user profile data
  get_user_profile_api(){
    return this._http.get(this.baseUrl+'v1/user-profile/false',{
      headers: this.header_token()
    }).map((res:any) => res.json());
  }  

  get_organization(){ 
    return this._http.get(this.baseUrl+'v1/get-organization/false', { 
      headers: this.header_token() 
    }).map((res:any)=> res.json());
  }

  // get all users list
  get_user_list_api() {
    return this._http.get(this.baseUrl+'v1/users/false', {
      headers: this.header_token()
    }).map((res:any) => res.json());
  }

  // won_lost using user_id
  won_lost_api(deal_id,data){
    return this._http.put(this.baseUrl+'v1/pipelines/deals/'+deal_id+'/won_lost/false',data, {
      headers: this.header_token()
    }).map((res:any)=> res.json());
  }

  // get all deals list using user_id
  get_deal(deal_id){
    return this._http.get(this.newUrl+'v1/get-deal/'+deal_id+'/false', {
      headers: this.header_token()
    });
  }

   /*list of custom fields */ 
  get_list_custom_field(value){
    return this._http.get(this.baseUrl+'v1/get-fields-alltable/'+'false', {
      headers: this.header_token()
    }).map((res:any) => res.json());
  }

   // update deal using user_id
  update_deal_api(deal_id, data){
    return this._http.put(this.baseUrl+'v1/pipelines/deals/'+deal_id+'/update/'+'false',data, {
      headers: this.header_token()
    }).map((res:any)=> res.json());
  }

   // update single field for organisation
  update_single_field_value_org(tableName, uuid, data){
    // uuid => person_uuid, org_uuid
    return this._http.put(this.baseUrl+'v1/contact-organization-person/'+tableName+'/'+uuid+'/update/'+'false', data, {
        headers: this.header_token()
    }).map((res:any) => res.json());
  }
  
}

@Injectable()
export class CompanyService {
  baseUrl: any;
  constructor(public cogitoUtil: CognitoUtil, public _http: Http){
    var url = new Baseurl()
    this.baseUrl = url.get_url();
  }

  header_token(){
    var token = this.cogitoUtil.get_session_Token();
    // var token = localStorage.getItem("aws-IdToken");
    let headers = new Headers();
    headers.append('Authorization', token);
    headers.append('Content-type', 'application/json');
    return headers;
  }

  // get single company details object using 'user_id'
  get_organization_(){
    return this._http.get(this.baseUrl+'v1/get-organization/false', { headers: this.header_token() })
  }

  get_organization_currency_field_api(is_show){
    return this._http.get(this.baseUrl+'v1/organization/currency/'+is_show+'/false', {
      headers: this.header_token()
    }).map((res:any) => res.json());
  }

  create_company_(data){
    return this._http.post(this.baseUrl+'v1/create/organization', data, {
      headers: this.header_token()
    });
  }

  // activity api 
  create_activity_api(data){
    return this._http.post(this.baseUrl+'v1/create/activity_field',data, {
      headers: this.header_token()
    }).map((res:any) => res.json());
  }

  get_company_activity_fields_api(){
    return this._http.get(this.baseUrl+'v1/get_activity_field/false', {
      headers: this.header_token()
    }).map((res:any) => res.json());
  }

  company_name_validation_api(data){
    return this._http.post(this.baseUrl+'v1/check_valid_companyname', data, {
      headers: this.header_token()
    }).map((res:any) => res.json());
  }
}


@Injectable()
export class PipelineService {
  baseUrl: any;
  constructor(public cogitoUtil: CognitoUtil, public _http: Http){
    var url = new Baseurl()
    this.baseUrl = url.get_url();
  }
  /*newUrl = 'https://zemkxckfml.execute-api.us-west-2.amazonaws.com/dev/';*/
  newUrl = 'https://abmrwnvmjb.execute-api.us-east-1.amazonaws.com/beta/';
  header_token(){
    var token = this.cogitoUtil.get_session_Token();
    // console.log(token);
    // var token = localStorage.getItem("aws-IdToken");
    let headers = new Headers();
    headers.append('Authorization', token);
    headers.append('Content-type', 'application/json');
    return headers;
  }

  // list of pipelines
  public get_pipeline_list_api() {
    return this._http.get(this.baseUrl+'v1/pipelines/list/false', {
      headers: this.header_token()
    }).map((res:any) => res.json());
  }

  public pipeline_stages_deals(pipeline_uuid, user_name, filter_by){
    // filter_by == 'user_name=', 'filter='
    return this._http.get(this.newUrl+'v1/pipelines/'+pipeline_uuid+'/deals/false?'+filter_by+user_name, {
      headers: this.header_token()
    }).map((res:any) => res.json());
  }

  pipeline_stage_filter_deals(pipeline_uuid, user_name, filter){
    return this._http.get(this.baseUrl+'v1/pipelines/'+pipeline_uuid+'/deals/false?'+filter+user_name, {
      headers: this.header_token()
    }).map((res:any) => res.json());
 }

  public move_pipeline_deal(move_data, stage_uuid){
    return this._http.put(this.baseUrl+'v1/pipelines/deals/stages/'+stage_uuid+'/move/false',move_data, {
      headers: this.header_token()
    }).map((res:any) => res.json());
  }
}

@Injectable()
export class AddActivityService {
  baseUrl: any;
  constructor(public cogitoUtil: CognitoUtil, public _http: Http){
    var url = new Baseurl()
    this.baseUrl = url.get_url();
  }

  header_token(){
    var token = this.cogitoUtil.get_session_Token();
    // var token = localStorage.getItem("aws-IdToken");
    let headers = new Headers();
    headers.append('Authorization', token);
    headers.append('Content-type', 'application/json');
    return headers;
  }

  get_activity_fa_fa_icon_api(){
    return this._http.get(this.baseUrl+'v1/get_activity_field/false',{
      headers: this.header_token()
    }).map((res:any) => res.json());
  }

   add_activity(deal_id,data){
    return this._http.post(this.baseUrl+'v1/create/activity/'+deal_id+'/false', data, {
      headers: this.header_token()
    }).map((res:any)=> res.json());
  }
  
  get_all_activity_listing_api(deal_id){
    return this._http.get(this.baseUrl+'v1/activity/'+deal_id, {
      headers: this.header_token()
    }).map((res:any)=> res.json());;
  }

   // edit and move created deal in another deal.
  edit_activity_new_api(activity_uuid, deal_uuid, data){
    return this._http.put(this.baseUrl+'v1/activity/edit/'+activity_uuid+'/'+deal_uuid+'/false', data, {
      headers: this.header_token()
    }).map((res:any) => res.json());
  }

  get_activities_listing_api(){
    return this._http.get(this.baseUrl+'v1/activity/'+'false', { headers: this.header_token()
    }).map((res:any) => res.json());
  }

   // click on check box set activity on true/false
  change_activity_status_api(activity_uuid, data){
    return this._http.put(this.baseUrl+'v1/activity/activity_complete_status/'+activity_uuid, data, {
      headers: this.header_token()
    }).map((res:any) => res.json());
  }

  delete_activity_api(activity_uuid){
    return this._http.put(this.baseUrl+'v1/activity/delete/'+activity_uuid+'/false', {},{
      headers: this.header_token()
    }).map((res:any)=> res.json());
  }

  }


@Injectable()
export class DealService {
  baseUrl: any;
  constructor(public cogitoUtil: CognitoUtil, public _http: Http){
    var url = new Baseurl()
    this.baseUrl = url.get_url();
  }
  
  /*newUrl = 'https://zemkxckfml.execute-api.us-west-2.amazonaws.com/dev/';*/
  newUrl = 'https://abmrwnvmjb.execute-api.us-east-1.amazonaws.com/beta/';

  header_token(){
    var token = this.cogitoUtil.get_session_Token();
    // var token = localStorage.getItem("aws-IdToken");
    let headers = new Headers();
    headers.append('Authorization', token);
    headers.append('Content-type', 'application/json');
    return headers;
  }

  // get list of currency
  get_organization_currency_field_api(is_show){
    return this._http.get(this.baseUrl+'v1/organization/currency/'+is_show+'/false', {
      headers: this.header_token()
    }).map((res:any) => res.json());
  }

  // create a new deals
  add_deal(pipeline_uuid, data){
    return this._http.post(this.newUrl+'v1/pipelines/'+pipeline_uuid+'/deals/create/false', data, {
      headers: this.header_token()
    }).map((res:any)=> res.json());
  }

  // update deal using user_id
  delete_deal_api(deal_uuid, data){
    return this._http.put(this.baseUrl+'v1/pipelines/deals/'+deal_uuid+'/delete/false', data, {
      headers: this.header_token()
    }).map((res:any)=> res.json());
  }

  update_deal_api(deal_uuid, data){
    return this._http.put(this.baseUrl+'v1/update/deal/'+deal_uuid+'/false', data, {
      headers: this.header_token()
    }).map((res:any)=> res.json());
  }

  get_statistics_deal_(dealstatus,startDate,endDate){
    return this._http.get(this.baseUrl+'v1/statistics/get-deal/'+dealstatus+'/'+startDate+'/'+endDate+'/'+'false', {
        headers: this.header_token()
    }).map((res:any) => res.json());
  }

  update_product_deal_detail_api(product_uuid, data){
    return this._http.put(this.newUrl+'v1/edit/deal_product/'+product_uuid+'/'+'false', data, {
      headers: this.header_token() 
    }).map((res:any) => res.json()); 
  }
  
  delete_product_deal(deal_uuid,product_uuid,data){
    return this._http.put(this.newUrl+'v1/delete/deal_product/'+deal_uuid+'/'+product_uuid+'/false', data, {
      headers: this.header_token() 
    }).map((res:any) => res.json()); 
  }
  
   product_list_api(deal_uuid){
     return this._http.get(this.newUrl+'v2/product_filter_app/'+deal_uuid+'/All'+'/false', {
      headers: this.header_token() 
    }).map((res:any) => res.json()); 
   }

  add_product_in_deal_detail(deal_uuid, data){ 
    return this._http.post(this.newUrl+'v1/add/products/'+deal_uuid+'/'+'false', data, {
      headers: this.header_token() 
    }).map((res:any) => res.json()); 
  }
}

@Injectable()
export class ContactService {
  // that class work for contact person and contact organization
  baseUrl: any;
  constructor(public cogitoUtil: CognitoUtil, public _http: Http){
    var url = new Baseurl()
    this.baseUrl = url.get_url();
  }

  /*newUrl = 'https://zemkxckfml.execute-api.us-west-2.amazonaws.com/dev/';*/
  newUrl = 'https://abmrwnvmjb.execute-api.us-east-1.amazonaws.com/beta/';

  header_token(){
    var token = this.cogitoUtil.get_session_Token();
    // var token = localStorage.getItem("aws-IdToken");
    let headers = new Headers();
    headers.append('Authorization', token);
    headers.append('Content-type', 'application/json');
    return headers;
  }

  // get list of all contact person
  contact_person_list_api(charT){
    return this._http.get(this.newUrl+'v1/contacts_persons_filter_app/'+charT+'/false', {
      headers: this.header_token()
    }).map((res:any) => res.json());
  }
  
  contact_organization_list_api(charT){
    return this._http.get(this.newUrl+'v1/contact_organization_filter_app/'+charT+'/false', {
      headers: this.header_token()
    }).map((res:any) => res.json());
  }

  // add new organization
  create_contact_organization_api(data){
    return this._http.post(this.baseUrl+'v1/create/contact-organization/false', data, {
      headers: this.header_token()
    });
  }
  // add new person
  create_person_api(data){
    return this._http.post(this.baseUrl+'v1/create/contacts_persons/false', data, {
      headers: this.header_token()
    });
  }

  get_deal(deal_id){
    return this._http.get(this.newUrl+'v1/get-deal/'+deal_id+'/false', {
      headers: this.header_token()
    });
  }
  
  person_deal_api(deal_id){
    return this._http.get(this.newUrl+'v1/get/contact-person/deals/'+deal_id+'/false', {
      headers: this.header_token()
    });
  }
  
  organisation_get_api(deal_id){
    return this._http.get(this.newUrl+'v1/get/contact-organization/deals/'+deal_id+'/false', {
      headers: this.header_token()
    }).map((res:any)=>res.json());
  }

  person_update_api(person_uuid, data){
    return this._http.put(this.baseUrl+'v1/contact-organization-person/contacts_persons/'+person_uuid+'/update-all/false', data,{
      headers: this.header_token()
    });
  }

  organisation_update_api(organisation_uuid, data){
    return this._http.put(this.baseUrl+'v1/contact-organization-person/contacts_organizations/'+organisation_uuid+'/update-all/false', data,{
      headers: this.header_token() }).map((res:any) => res.json());}

  person_delete_api(person_uuid){
    return this._http.put(this.baseUrl+'v1/delete/contact-person/'+person_uuid, '',{
      headers: this.header_token()
    }).map((res:any)=> res.json());
  }
  organisation_delete_api(organisation_uuid){
    return this._http.put(this.baseUrl+'v1/delete/contact-organization/'+organisation_uuid,'',{
      headers: this.header_token()
    }).map((res:any)=> res.json());
  }
 }

@Injectable()
export class StripeServices{
  baseUrl: any;
  constructor(public cogitoUtil: CognitoUtil, public _http: Http){
    // var url = new Baseurl()
    this.baseUrl = 'https://abmrwnvmjb.execute-api.us-east-1.amazonaws.com/beta/v1/';
  }

  header_token(){
    let headers = new Headers();
    headers.append('Content-type', 'application/json');
    return headers;
  }
  
  // this method call into company registration component.
  create_trail_stripe_api(data){
    return this._http.post(this.baseUrl+'trial/subscription', data).map((res:any) => res.json());
  }

  get_billing_info_api(org_uuid){
    return this._http.get(this.baseUrl+'retrieve/subscription/'+org_uuid,{headers: this.header_token()}).map((res:any) => res.json());
  }
}