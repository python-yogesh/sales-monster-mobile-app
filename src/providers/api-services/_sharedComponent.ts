export class SharedComponent {
  constructor(){}
  //yyyy-mm-dd
  dateToYMD(date) {
    var d = date.getDate();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    return y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
  }

   dateToDMY(date) {
    var d = date.getDate();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    return (d <= 9 ? '0' + d : d) + '-' + (m<=9 ? '0' + m : m) + '-' + y;
  }


  dateToHMS(timestamp){
    var time = timestamp;
    var minutes = time.getMinutes();
    var ampm = time.getHours()<=11 ? 'AM' : 'PM';
    var minute = (minutes<=9) ? '0' + minutes : minutes;
    var hours = time.getHours()%12;
    var _time = (hours <= 9 ? ('0'+hours=='00' ? '12': '0'+hours) : hours) + ':' + minute + ' ' + ampm;
    return _time; // return this type of format 01:00 AM/PM
  }

  dateToHMS_24(timestamp){
    var time = timestamp;
    var minutes = time.getMinutes();
    var minute = (minutes<=9) ? '0' + minutes : minutes;
    var hours = time.getHours();
    var _time = (hours <= 9 ? '0'+hours : hours) + ':' + minute;
    return _time; // return this type of format 23:01
  }

  addHours(){
    var milliseconds = new Date().getTime() + (1 * 60 * 60 * 1000);
    var later = new Date(milliseconds);
    return later;
  }
}