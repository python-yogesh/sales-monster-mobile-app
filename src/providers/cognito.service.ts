import {Injectable} from "@angular/core";
import {_POOL_DATA} from "./properties.service";

declare let AWS: any;
declare let AWSCognito: any;

export class RegistrationUser {
    name: string;
    email: string;
    password: string;
}

export interface CognitoCallback {
    cognitoCallback(message: string, result: any): void;
}

export interface CognitoCallback_Confirm_Code {
    CognitoCallback_confirm_code(message: string, result: any): void;
}

export interface LoggedInCallback {
    isLoggedInCallback(message: string, loggedIn: boolean): void;
}

export interface CognitoCallback_Login {
  cognitoCallback_login(message: string, result: any): void;
}

export interface CognitoCallback_reSendCode {
  cognitoCallback_resend_code(message: string, result: any): void;
}

export interface Callback {
    callback(): void;
    callbackWithParam(result: any): void;
}

@Injectable()
export class CognitoUtil {

    constructor() {
        console.log("CognitoUtil constructor");
        setInterval(() => {
            this.getIdToken(this);
        }, 4000)
    }
    callback(){}
    callbackWithParam(res:any){
        
    }

    getUserPool() {
        return new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(_POOL_DATA);
    }

    getCurrentUser() {
        return this.getUserPool().getCurrentUser();
    }


    getCognitoIdentity(): string {
        return AWS.config.credentials.identityId;
    }

    getAccessToken(callback: Callback): void {
        if (callback == null) {
            throw("callback in getAccessToken is null...returning");
        }
        this.getCurrentUser().getSession(function (err, session) {
            if (err) {
                console.log("Can't set the credentials:" + err);
                callback.callbackWithParam(null);
            }

            else {
                if (session.isValid()) {
                    callback.callbackWithParam(session.getAccessToken().getJwtToken());
                }
            }
        });
    }

    getIdToken(callback: Callback): void {
        if (callback == null) {
            throw("callback in getIdToken is null...returning");
        }
        if (this.getCurrentUser() != null)
            this.getCurrentUser().getSession(function (err, session) {
                if (err) {
                    console.log("Can't set the credentials:" + err);
                    callback.callbackWithParam(null);
                }
                else {
                    if (session.isValid()) {
                        callback.callbackWithParam(session.getIdToken().getJwtToken());
                    } else {
                        console.log("Got the id token, but the session isn't valid");
                    }
                }
            });
        else
            callback.callbackWithParam(null);
    }

    getRefreshToken(callback: Callback): void {
        if (callback == null) {
            throw("callback in getRefreshToken is null...returning");
        }
        this.getCurrentUser().getSession(function (err, session) {
            if (err) {
                console.log("Can't set the credentials:" + err);
                callback.callbackWithParam(null);
            }

            else {
                if (session.isValid()) {
                    callback.callbackWithParam(session.getRefreshToken());
                }
            }
        });
    }

    get_session_Token(){
        var session_token;
        if (this.getCurrentUser() != null)
            this.getCurrentUser().getSession(function (err, session) {
                if (err) {
                    console.log("CognitoUtil: Can't set the credentials:" + err);
                    session_token = null;
                }
                else {
                    if (session.isValid()) {
                        session_token = session.getIdToken().getJwtToken();
                    }
                }
            });
        else
            session_token = null;
        return session_token;
    }
}






