import {Injectable} from "@angular/core";
import {CognitoCallback_Login, CognitoCallback, CognitoUtil, LoggedInCallback, CognitoCallback_Confirm_Code} from "./cognito.service";
import {EventsService} from "./events.service";
//declare let AWS: any;
declare let AWSCognito: any;

@Injectable()
export class UserLoginService {

    constructor(public cUtil: CognitoUtil, public eventService: EventsService) {
       /* console.log("eventservice1: " + eventService);*/
    }

    authenticate(username: string, password: string, callback: CognitoCallback_Login) {
        //let mythis = this;
        // Need to provide placeholder keys unless unauthorised user access is enabled for user pool
        AWSCognito.config.update({accessKeyId: 'anything', secretAccessKey: 'anything'})

        let authenticationData = {
            Username: username,
            Password: password,
        };
        let authenticationDetails = new AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);

        let userData = {
            Username: username,
            Pool: this.cUtil.getUserPool()
        };

        /*console.log("Authenticating the user");*/
        let cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                callback.cognitoCallback_login(null, result);
                //mythis.eventService.sendLoggedInEvent();
            },
            onFailure: function (err) {
                callback.cognitoCallback_login(err.message, null);
            },
        });
    }

    forgotPassword(username: string, callback: CognitoCallback) {
        let userData = {
            Username: username,
            Pool: this.cUtil.getUserPool()
        };

        let cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);

        cognitoUser.forgotPassword({
            onSuccess: function (result) {

            },
            onFailure: function (err) {
                callback.cognitoCallback(err.message, null);
            },
            inputVerificationCode() {
                callback.cognitoCallback(null, null);
            }
        });
    }

    // after login change password authorized user
    changePassword(oldPassword: string, newPassword: string, callback: CognitoCallback){
      let cognitoUser = this.cUtil.getCurrentUser();
      if (cognitoUser != null) {
            cognitoUser.getSession(function (err, session) {
              if (err) {
                  console.log("UserLoginService: Couldn't get the session: " + err, err.stack);
              }
              else{
                cognitoUser.changePassword(oldPassword, newPassword, function(err, result) {
                  if (err) {
                    callback.cognitoCallback(err.message, null); 
                  }else{
                    callback.cognitoCallback(null, result);
                  }
                });
              }
            });
        }
    }

    confirmNewPassword(email: string, verificationCode: string, password: string, callback: CognitoCallback_Confirm_Code) {
        let userData = {
            Username: email,
            Pool: this.cUtil.getUserPool()
        };

        let cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);

        cognitoUser.confirmPassword(verificationCode, password, {
            onSuccess: function (result) {
                callback.CognitoCallback_confirm_code(null, result);
            },
            onFailure: function (err) {
                callback.CognitoCallback_confirm_code(err.message, null);
            }
        });
    }

    logout() {
        //console.log("Logging out");
        this.cUtil.getCurrentUser().signOut();
        // this.eventService.sendLogoutEvent();
    }

    isAuthenticated(callback: LoggedInCallback) {
        if (callback == null)
            throw("Callback in isAuthenticated() cannot be null");

        console.log("Getting the current user");
        let cognitoUser = this.cUtil.getCurrentUser();

        if (cognitoUser != null) {
            cognitoUser.getSession(function (err, session) {
                if (err) {
                    console.log("Couldn't get the session: " + err, err.stack);
                    callback.isLoggedInCallback(err, false);
                }
                else {
                    console.log("Session is valid: " + session.isValid());
                    callback.isLoggedInCallback(err, session.isValid());
                }
            });
        } else {
            callback.isLoggedInCallback("Can't retrieve the CurrentUser", false);
        }
    }
}