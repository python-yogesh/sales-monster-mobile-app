export let _REGION = "us-east-1";
export let _IDENTITY_POOL_ID = "us-east-1:861f63a1-089e-4844-89c6-0d9345a1010f";
export let _USER_POOL_ID = "us-east-1_WOduEimsu";
export let _CLIENT_ID = "6hrnlg8otv71rijqkheg0uqciu";
export let _MOBILE_ANALYTICS_APP_ID = "9aa5408b8c024a919da7f4f2d8f62918";

export let _POOL_DATA = {
    UserPoolId: _USER_POOL_ID,
    ClientId: _CLIENT_ID
};