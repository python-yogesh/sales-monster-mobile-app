import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, Events } from 'ionic-angular';
import { AddDealPage } from '../add-deal/add-deal';
import { AddActivityPage } from '../add-activity/add-activity';
import { UserService, PipelineService,AddActivityService } from '../../providers/api-services/user.service';
import { LostReasonPage } from '../lost-reason/lost-reason';
import { PersonDetailPage } from '../person-detail/person-detail';
import { OrganisationDetailPage } from '../organisation-detail/organisation-detail';
import { CustomFieldPage } from '../custom-field/custom-field';
import { ProductDetailPage } from '../product-detail/product-detail'


declare var $:any;

@IonicPage()
@Component({
  selector: 'page-deal-detail',
  templateUrl: 'deal-detail.html',
})

export class DealDetailPage {
  @ViewChild('pageSlider') pageSlider: Slides;
  @ViewChild('slides') slides: Slides;
  tabs: any = '0';
  index:any = '0';
  dealindex:any = '';

  private detail_list;
  private pipeline_stages;
  /*private deal_person_status;*/
  private stages_index;
  private deal;
  private selected_pipeline_id;
  private deal_uuid;
  private contact_organization_detail;
  private contact_person_uuid;
  private activities_status_false;
  private activities_status_true;
  private activity_length;


  // private index_fo_stages;
  constructor(public navCtrl: NavController, public navParams: NavParams, public authUser: UserService, public events: Events, public pipeline_services: PipelineService, public addactivityService:AddActivityService) {
    this.detail_list = this.navParams.get('detail_list_pipeline');
    this.pipeline_stages = this.navParams.get('list_of_stages');
    /*console.log('aa', this.pipeline_stages);*/
    this.stages_index = this.navParams.get('stagesindex');
    
    /*console.log(this.stages_index);*/
    this.selected_pipeline_id = this.navParams.get('selected_pipeline_id');
    events.subscribe('get_deal_list',()=>{
      this.get_pipeline_list();
    });

    events.subscribe('get_deal',(stagesindex)=>{
      this.stages_index = stagesindex;
      this.get_pipeline_list();
    });
    this.get_pipeline_list(); 
    this.get_created_activity();

    events.subscribe('get_activity_list',()=>{
        this.get_created_activity();
      });
  }

  isBigEnough(element) {
    return (element.is_deleted==true); 
  }

  ionViewDidLeave(){
    $('.ios .scroll-bar .scroll-content').css('margin-top', '100px');
    $('.md .scroll-bar .scroll-content').css('margin-top', '100px');
  }

  ionViewDidEnter(){
    this.slides.slideTo(this.stages_index);
  }

  slideChanged() {
    /*let currentIndex = this.slides.getActiveIndex();*/
    /*console.log('Current index is', currentIndex);*/
  }

  selectTab(index) {
    this.pageSlider.slideTo(index);
  }

  changeWillSlide($event) {
    this.tabs = $event._snapIndex.toString();
  }

  push_on_product(deal_uuid){
    this.navCtrl.push(ProductDetailPage,{
      'deal_uuid': deal_uuid,
    });
  }

  // get created activity listing
  private addActivity_length;
  private activity_data_list;
  get_created_activity(){
    this.addactivityService.get_all_activity_listing_api(this.deal_uuid).subscribe(data=>{
      this.activity_data_list = data;
      this.activities_status_false = data.filter((x)=> { return x.activity_complete_status === false});
      this.addActivity_length = this.activities_status_false.length;
      this.activities_status_true = data.filter((y)=> { return y.activity_complete_status === true });
      this.activity_length = this.activities_status_true.length;
    },(err) => console.error(err) );
  }

  // change stages tap to screen
  changeSlide($event){
    this.index = $event._snapIndex.toString();
    this.stages_index = this.slides.getActiveIndex();
    var data = this.pipeline_stages.filter((i, index)=>{ return this.stages_index === index; });
    var obj = {};
    obj = data[0];
    this.move_drop_data(obj);
  }

  // click on check box set activity on true/false
  change_activity_status(activity_uuid, value: boolean){
    // console.log(activity_uuid, value)
    var data = { 'activity_complete_status': value };
    this.addactivityService.change_activity_status_api(activity_uuid, data).subscribe(data=>{
      if (data.ResponseMetadata.HTTPStatusCode == 200) {
        this.get_created_activity();
      }
    })
  }

  // calling, click on pencile icon
  pushOnAddDealEdit(){
    this.navCtrl.push(AddDealPage, {
      'single_deal_obj': this.deal,
      'list_of_pipeline': this.navParams.get('list_of_pipeline'),
      'list_of_stages': this.navParams.get('list_of_stages'),
      'selected_pipeline_id': this.deal.pipeline_uuid,
      'selected_stage_uuid': this.deal.stage_uuid,
      'product_quantity':this.product_qty,
    });
  }

  push_on_activity_page(){
    this.navCtrl.push(AddActivityPage, {
      'detail_list': this.detail_list,
      'pipeline_stages': this.pipeline_stages,
      'value':0,
    });
  }

  push_on_edit_activity(data){
    this.navCtrl.push(AddActivityPage, {
      'single_activity_detail': data,
      'value':1,
      'detail_list': this.detail_list,
      'pipeline_stages': this.pipeline_stages,
    });
  }

  private product_list = [];
  private product_qty: number = 0;
  // get single deal detail data
  get_pipeline_list(){
      this.authUser.get_deal(this.detail_list.uuid).subscribe(data=>{
        this.deal = data.json()['Item'];
        this.deal_uuid = data.json()['Item'].uuid;
        var deal_object = data.json()['Item'];
        console.log(deal_object);
        if(this.deal!=undefined){
          this.contact_person_uuid = this.deal.contact_person_uuid.uuid
          this.contact_organization_detail = this.deal.contact_organization_uuid;
        } 
        // put value inactivity form
        this.list_of_fields(deal_object);

        this.product_list = deal_object.is_product ? deal_object.deal_product_response : [];
        this.product_qty = 0;
        this.product_list.forEach((x)=>{
        this.product_qty = this.product_qty + x.qty;
        });
        /*console.log('DEAL DETAILS',this.deal);*/
      }, error=> console.log('error=',error));
  }

  

  private contacts_deal_fields_obj; // form variable
  private contacts_deal_fields;
  private deal_field_api_key;
  private deal_add_field_dev;
  list_of_fields(deal_obj){
    this.deal_field_api_key = '';
    this.authUser.get_list_custom_field('True').subscribe(data=>{
      /*console.log('custom fields', data);*/
      this.deal_add_field_dev = false;
      // deal custom field data
      this.contacts_deal_fields = data.deal_fields.filter(this.isBigEnough);
      var contacts_deal_fields = data.deal_fields.filter(this.isBigEnough);
      var contact_deal_arr = [];
      for (var j in contacts_deal_fields){
        var value = deal_obj[contacts_deal_fields[j].field_api_key] != undefined ? deal_obj[contacts_deal_fields[j].field_api_key] : ''
        let obj = {
          'field_api_key': contacts_deal_fields[j].field_api_key,
          'label': contacts_deal_fields[j].label,
          'value': value,
          'is_type': contacts_deal_fields[j].is_type,
          'multiple_value': contacts_deal_fields[j].multiple_value,
          'deal_uuid': deal_obj.uuid
        }
        contact_deal_arr.push(obj);
      }
      /*console.log('contact_deal_arr', contact_deal_arr)*/
      this.contacts_deal_fields_obj = contact_deal_arr;
    });
  }


  update_reopen(lostDeal){
    var reopenDeal = {deal_reopen:lostDeal};
    this.authUser.won_lost_api(this.detail_list.uuid,reopenDeal).subscribe(data=>{
       this.events.publish('person_list:listing');
       this.events.publish('organisation_detail:listing');
       this.get_pipeline_list();
    },(err) => console.error(err) );
  }

  update_won(wonDeal){
    var won = {deal_won:wonDeal};
    this.authUser.won_lost_api(this.detail_list.uuid,won).subscribe(data=>{
      // this.events.publish('pipeline:stage', this.selected_pipeline_id);
      // this.events.publish('pipeline:stage_', this.selected_pipeline_id);
      this.events.publish('person_list:listing');
      this.events.publish('organisation_detail:listing');
      this.get_pipeline_list();
    },(err) => console.error(err) );
  }
  
  lost_reason(){
    this.navCtrl.push(LostReasonPage, {
      'deal_uuid': this.detail_list.uuid,
      'pipeline_stages': this.pipeline_stages,
      
    });
  }

  custom_fields(deal_field){
    this.navCtrl.push(CustomFieldPage, {
      'deal_uuid': this.detail_list.uuid,
      'pipeline_stages': this.pipeline_stages,
      'deal_field':deal_field,
      'value': 0,
    });
  }


  on_person(){
    this.navCtrl.push(PersonDetailPage, {
      'detail_list': this.detail_list,
      'pipeline_stages': this.pipeline_stages,
      'deal_person_uuid': this.contact_person_uuid,
      'list_of_pipeline': this.navParams.get('list_of_pipeline'),
      'list_of_stages': this.navParams.get('list_of_stages'),
      'selected_pipeline_id': this.deal.pipeline_uuid,
      'selected_stage_uuid': this.deal.stage_uuid,
      'stagesindex': this.stages_index,
    });
  }

  on_organisation(){
    this.navCtrl.push(OrganisationDetailPage, {
      'contact_organization_detail': this.contact_organization_detail,
      'pipeline_stages': this.pipeline_stages,
      'list_of_pipeline': this.navParams.get('list_of_pipeline'),
      'list_of_stages': this.navParams.get('list_of_stages'),
      'selected_pipeline_id': this.deal.pipeline_uuid,
      'selected_stage_uuid': this.deal.stage_uuid,
      'stagesindex': this.stages_index,
      'detail_list': this.detail_list,
    });
  }

  
  // drag drop data in stages
  move_drop_data(stage_uuid){ // event is a stage data
    var data = {'uuid': this.detail_list.uuid} // move in other stage uuid
    this.pipeline_services.move_pipeline_deal(data, stage_uuid.uuid).subscribe(data=>{
      // this.events.publish('pipeline:stage', this.selected_pipeline_id);
    });
  }
}
