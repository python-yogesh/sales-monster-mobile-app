import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, Events} from 'ionic-angular';
import { AddActivityService,UserService,ContactService,CompanyService } from '../../providers/api-services/user.service';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { SearchPage } from '../search/search';
import { SharedComponent } from '../../providers/api-services/_sharedComponent'
import { CognitoUtil } from '../../providers/cognito.service'
import { UserLoginService } from '../../providers/userLogin.service';

@IonicPage()
@Component({
  selector: 'page-add-activity',
  templateUrl: 'add-activity.html',
})

export class AddActivityPage {
  private fa_fa_icon_list;
  private activity_form: FormGroup;
  private detail_list;
  private user_list_data;
  private owner_id;
  private selected_owner_id;
  private owner_name;
  private deal_title;
  private _sharedComponent = new SharedComponent();
  private pipelinestages;
  private starts_at;
  private ends_at;
  private single_activity_detail;
  private value;
  /*private Dealtitleoption;*/
  Dealtitleoption: any = '0';
  
  private activity_data = { activityTitle:'',activity_fa_fa_icon:'', activity_start_time:'',activity_end_time:'',
    activity_date:'',owner_name:'',contact_person_name:'',contact_organization_name:'',deal_title:'',activity_editor:''
  }
  

  public event = {
    timeStarts: this._sharedComponent.dateToHMS_24(new Date()),
   /* newdate = this._sharedComponent.addHours(new Date()),*/
    timeEnds : this._sharedComponent.dateToHMS_24(this._sharedComponent.addHours()),
  }

  constructor(public navCtrl: NavController, public navParams: NavParams,public activity_services: AddActivityService,
    public formBuilder: FormBuilder, public user_service:UserService,public contact_services: ContactService,public loadingCtrl: LoadingController,
    public toastCtrl: ToastController, public events:Events, public connito_utils: CognitoUtil, public user_login_service: UserLoginService, public activity_add:CompanyService) {
    
    this.pipelinestages = this.navParams.get('pipeline_stages');
    this.get_user_list();

    this.detail_list = this.navParams.get('detail_list');
    /*console.log(this.detail_list);*/
    this.get_fa_icon_listing();
    
    
    /*activity detail*/
    this.single_activity_detail = this.navParams.get('single_activity_detail');
    this.value = this.navParams.get('value');
    if(this.value == 1){
      this.activity_data.activity_date = this.single_activity_detail.activity_date;
      
      this.activity_data.activity_start_time = this._sharedComponent.dateToHMS_24(new Date(this.single_activity_detail.start_at));
      this.activity_data.activity_end_time =this._sharedComponent.dateToHMS_24(new Date(this.single_activity_detail.end_at));
      this.activity_data.contact_person_name = this.single_activity_detail.contact_person_name;
      this.activity_data.contact_organization_name = this.single_activity_detail.contact_organization_name;
      this.activity_data.deal_title = this.single_activity_detail.deal_title;
      this.activity_data.activity_editor = this.single_activity_detail.activity_editor;
      this.activity_data.activityTitle = this.single_activity_detail.activityTitle;
    }
    else{
      this.activity_data.activity_date = this._sharedComponent.dateToYMD(new Date());
      this.activity_data.activity_start_time = this._sharedComponent.dateToHMS_24(new Date());
      this.activity_data.contact_person_name = this.detail_list.contact_person_uuid;
      this.activity_data.contact_organization_name = this.detail_list.contact_organization_uuid;
      this.activity_data.deal_title = this.detail_list.deal_title;
      this.activity_data.activity_end_time = this._sharedComponent.dateToHMS_24(this._sharedComponent.addHours());
    } 
    

    /*this.end_date = this._sharedComponent.dateToYMD(new Date(end_date));*/


    this.initialize_form();
    events.subscribe('contact:org', (userEventData) => {
      
      this.activity_form.controls['contact_organization_name'].setValue(userEventData.org_name);
      /*this.activity_form.controls['contact_organization_uuid'].setValue(userEventData.org_uuid);*/
      this.deal_title = userEventData.org_name + ' deal';
      // this.org_name = userEventData;
    });
    events.subscribe('contact:person', (userEventData) => {
      this.activity_form.controls['contact_person_name'].setValue(userEventData.person_name);
      /*this.activity_form.controls['contact_person_uuid'].setValue(userEventData.person_uuid);*/
      this.deal_title = userEventData.person_name + ' deal';
    });
    this.owner_id = this.user_login_service.cUtil.getCurrentUser().username; 
    this.selected_owner_id = this.user_login_service.cUtil.getCurrentUser().username;
 }

viewDate_activity: Date = new Date();
initialize_form(){
  this.activity_form = this.formBuilder.group({
        'activityTitle': [''],
        "activity_fa_fa_icon": [''],
        'activity_field_uuid': [''],
        'activity_date': [''],
        'activity_start_time': [],
        'activity_end_time': [''], 
        'owner_name':[''],
        'owner_id': [''],
        'activity_editor' :[''],
        'contact_person_name':[''],
        'contact_organization_name': [''],
        'deal_title': ['', Validators.compose([Validators.required])],
        'local_timezone': [''],
        'start_at' : [''],
        'end_at' : [''],
        'activity_tooltip_name': [''],
      });
    }

activity_fa_icon(tooltip_name, fa_fa_icon, uuid){
  this.activity_form.controls.activity_fa_fa_icon.setValue(fa_fa_icon);
  this.activity_form.controls.activity_field_uuid.setValue(uuid);
}

fa_icon
private owner_name_new;
get_fa_icon_listing(){ 
  this.activity_services.get_activity_fa_fa_icon_api().subscribe(data=>{ 
  this.fa_icon=true;
  this.fa_fa_icon_list = data.Items.filter((x)=>{ return x.is_activate === true });
  if(this.value==0){
    this.activity_data.activityTitle = this.fa_fa_icon_list[0].label;
  }
}); 
}
get_user_list(){ 
  this.user_service.get_user_list_api().subscribe(data=>{
  this.user_list_data = data.users; 
},(err) => console.error(err) ); }

// push on SearchPage when we click on input button
push_on_person_search_page(){
  this.navCtrl.push(SearchPage, {
    'value': 0
  });
}

// push on SearchPage when we click on input button
contact_org_search_page(){
  this.navCtrl.push(SearchPage, {
    'value': 1
  });
}
  
/*push_on_search_page(){
  this.navCtrl.push(SearchPage, {
    'pipelinestages': this.pipelinestages,
  });
}*/

private loader;
presentLoading() {
  this.loader = this.loadingCtrl.create({
    content: "Please wait...",
  });
  this.loader.present();
}
ionViewDidLoad() {
/*console.log('ionViewDidLoad AddActivityPage');*/
}

save_activity()
{
  this.activity_form.controls.owner_name.setValue(this.owner_name_new);
  this.activity_form.controls.start_at.setValue(new Date(this.starts_at));
  var data =this.activity_form.value;
  data["local_timezone"] = Intl.DateTimeFormat().resolvedOptions().timeZone;
  this.starts_at = data.activity_date + " " + data.activity_start_time;
  this.ends_at = data.activity_date + " "+ data.activity_end_time;
  data.start_at = new Date(this.starts_at);
  data.end_at = new Date(this.ends_at);
  data.activity_start_time = this._sharedComponent.dateToHMS(data.start_at);
  data.activity_end_time = this._sharedComponent.dateToHMS(data.end_at);
  data.activity_tooltip_name = data.activityTitle;
  if (this.activity_form.valid) {
    if(data.activity_end_time > data.activity_start_time){
      if(this.value==0){
      this.activity_services.add_activity(this.detail_list.uuid,this.activity_form.value).subscribe(data=>{
        if (data.ResponseMetadata.HTTPStatusCode == 200) {
          alert('activity added Successfully');
          this.events.publish('get_activity_list');
          this.navCtrl.pop();
        }
      }, (err)=>{
        this.loader.dismiss();
      });
    }
    else{
      this.activity_services.edit_activity_new_api(this.single_activity_detail.uuid, this.single_activity_detail.deal_uuid,this.activity_form.value).subscribe(data=>{
          if (data.ResponseMetadata.HTTPStatusCode == 200) {
            alert('activity updated successfully');
            this.events.publish('get_activity_list');
            this.navCtrl.pop();
          }
        });
    }
    }
    else{
      alert('End time should be greater than start time');
    }
    }
 /*console.log(data);*/
}

/*delete_activity_api(){
  this.activity_services.delete_activity_api(this.single_activity_detail.uuid).subscribe(data=>{
    if (data.ResponseMetadata.HTTPStatusCode == 200) {
      alert('activity deleted successfully');
      this.events.publish('get_activity_list');
      this.navCtrl.pop();
    }
  });
}
*/
get_owner_name(event){
  this.owner_name=event.currentTarget.selectedOptions[0].text;
}

onSelectChange(event){
  this.Dealtitleoption = event;
}

}