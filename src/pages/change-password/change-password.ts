import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserLoginService } from "../../providers/userLogin.service";
import { ValidationService } from '../../providers/validationServices';



/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {
  myform: FormGroup;
  newPassword: string;
  oldPassword: string;
  errorMessage: string;
  confirm_password: string;
  password_type: string = 'password';
  new_password: string = 'password';
  confirm_password_type: string = 'password';

  constructor(public navCtrl: NavController, public navParams: NavParams,public userLoginService: UserLoginService, public formBuilder: FormBuilder,public toastCtrl: ToastController,) {
    this.createForm();
  }
  createForm() {
  this.myform = this.formBuilder.group({
    oldPassword: ['', Validators.compose([Validators.required])],
    newPassword: ['', Validators.compose([Validators.required])],
    confirm_password: ['',  Validators.compose([Validators.required])],
  }, {'validator': ValidationService.isMatching});
  }

  ngOnInit(){}
  change_password(){
    // console.log(this.myform.valid);
    if (this.myform.valid) {
      this.userLoginService.changePassword(this.oldPassword, this.newPassword, this);
      this.presentToast('Change Password successfully');
    }
  }
  newPasswordToggle(){
    this.new_password = this.new_password === 'text' ? 'password' : 'text';
  }
  cognitoCallback(message: string, result: string){
    message!=null ? alert(message) : alert(result);
    if (message==null) {
      this.createForm();
    }
  }

   presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
    });
    toast.present();
  }
  togglePasswordMode() {   
    this.password_type = this.password_type === 'text' ? 'password' : 'text';
  }
  confirmPasswordToggle(){
    this.confirm_password_type = this.confirm_password_type === 'text' ? 'password' : 'text';
  }
  ionViewDidLoad() {
    // console.log('ionViewDidLoad ChangePasswordPage');
  }
}
