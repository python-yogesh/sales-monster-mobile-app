import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides,Events,LoadingController } from 'ionic-angular';
import { ContactService } from '../../providers/api-services/user.service';
/*import { AddActivityPage } from '../add-activity/add-activity';
*/
import { AddDealPage } from '../add-deal/add-deal';
import { AddPersonPage } from '../add-person/add-person';
import { OrganisationDetailPage } from '../organisation-detail/organisation-detail'
import { DealDetailPage } from '../deal-detail/deal-detail';
import { AddActivityPage } from '../add-activity/add-activity';
import { PipelineService,UserService } from "../../providers/api-services/user.service";
import { CustomFieldPage } from '../custom-field/custom-field';




@IonicPage()
@Component({
  selector: 'page-person-detail',
  templateUrl: 'person-detail.html',
})
export class PersonDetailPage {
	@ViewChild('pageSlider') pageSlider: Slides;
	@ViewChild('slides') slides: Slides;
	private detail_list;
	private pipeline_stages;
	private deal;
	private person_uuid;
  private won;
  private lost;
  private open;
  private open_length;
  private deals;
  private person_data;
  private organizationdata = {'organization_name':''}
  private won_length;
  private lost_length;
  private phone;
  private email;
  private work;
  private stages_index;
  private currency_data;
  private stages_list;
 
	pet: string = "timeline";
	index:any = '0';
	tabs: any = '0';
  
  constructor(public navCtrl: NavController, public navParams: NavParams ,public contactservice: ContactService,public events: Events,public pipeline_service: PipelineService,public authUser: UserService,public loadingCtrl: LoadingController) {
  	this.detail_list = this.navParams.get('detail_list');
    this.pipeline_stages = this.navParams.get('pipeline_stages');
    this.person_uuid  = this.navParams.get('deal_person_uuid');
    this.stages_index = this.navParams.get('stagesindex');
    this.get_person();
    this.deals = "general";
    events.subscribe('person_list:listing', () => {
      this.get_person();
    });
    this.get_pipeline_list();
  }

     
  changeWillSlide($event) {
    this.tabs = $event._snapIndex.toString();
  } 

  isBigEnough(element, index, array) {
    return (element.is_deleted==true); 
  }
    
  result: any;
  get_person(){
    this.contactservice.person_deal_api(this.person_uuid).subscribe(data=>{
      // console.log('data_', data.json());
      this.person_data = data.json()['contactPerson'];
      if (this.person_data.person_phone!=undefined) {
        this.phone = data.json()['contactPerson'].person_phone[0].phone;
        this.work = data.json()['contactPerson'].person_phone[0].selF;
      }
      if (this.person_data.person_email!=undefined) {
        this.email = data.json()['contactPerson'].person_email[0].email;
      }
      this.organizationdata = data.json()['contactOrganization'];
      this.deal = data.json()['deals'];
      
      this.list_of_fields(this.person_data);

      var deal_data = data.json()['deals']
      this.won = deal_data.filter((x)=>{ return x.dealstatus==2 });
      this.lost = deal_data.filter((y)=>{ return y.dealstatus==3 });
      this.open = deal_data.filter((z)=>{ return z.dealstatus==1 });
      
      this.open_length = this.open.length;
      this.won_length = this.won.length;
      this.lost_length = this.lost.length;
      
      this.currency_data = data.json()['currency_data'];
    },(err)=> {
        if (err.ok == false) {
          // this.get_pileline_stages_data(this.pipeline_uuid);
        }
    });
  }

  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    this.loader.present();
  }


  private contacts_persons_fields_obj = []; // form variable
  private contacts_persons_fields;
  private person_add_field_dev;
  private person_field_api_key;

  list_of_fields(deal_obj){
    this.deal = deal_obj;
    this.person_field_api_key = '';
    this.authUser.get_list_custom_field('True').subscribe(data=>{
      // console.log('custom fields', data)
      this.person_add_field_dev = false;
      // person custom fields data
      this.contacts_persons_fields = data.contacts_persons_fields.filter(this.isBigEnough);
      var contacts_persons_fields = data.contacts_persons_fields.filter(this.isBigEnough);
      var contact_person_arr = [];
      for (var j in contacts_persons_fields){
        var value = deal_obj[contacts_persons_fields[j].field_api_key] != undefined ? deal_obj[contacts_persons_fields[j].field_api_key] : ''
        let obj = {
          'field_api_key': contacts_persons_fields[j].field_api_key,
          'label': contacts_persons_fields[j].label,
          'value': value,
          'is_type': contacts_persons_fields[j].is_type,
          'multiple_value': contacts_persons_fields[j].multiple_value,
          'person_uuid': deal_obj.uuid
        }
        contact_person_arr.push(obj);
      }
      // console.log('contact_person_arr', contact_person_arr)
      this.contacts_persons_fields_obj = contact_person_arr;
    });
  }
  
   custom_fields(deal_field){
    this.navCtrl.push(CustomFieldPage, {
      'deal_field':deal_field,
      'value':1,
    });
  }

  push_on_org(){
    this.navCtrl.push(OrganisationDetailPage,{
      'contact_person_detail':this.person_data,
      'contact_organization_detail':this.organizationdata,
      'list_of_pipeline': this.pipeline_list,
      'list_of_stages': this.stages_list,
      'selected_pipeline_id':this.navParams.get('selected_pipeline_id'),
      'selected_stage_uuid': this.navParams.get('selected_stage_uuid'),
      'stagesindex': this.stages_index,
    });
  }

  push_on_adddeal_page(){
    this.navCtrl.push(AddDealPage,{
      'list_of_pipeline': this.pipeline_list,
      'list_of_stages': this.stages_list,
      'selected_pipeline_id':this.selected_pipeline_add_deal,
      'selected_stage_uuid': this.selected_stage_uuid_for_add_deal,
    });
  }

  push_on_activity_page(){
    this.navCtrl.push(AddActivityPage, {
      'detail_list': this.detail_list,
      'pipeline_stages': this.pipeline_stages,
    });
  }

  
  
  open_detail_page(newdata){
    var pipelineuuid = newdata.pipeline_uuid;
    this.presentLoading();
    this.pipeline_service.pipeline_stages_deals(pipelineuuid, 'all', 'user_name=').subscribe(data=>{
      this.loader.dismiss();
      this.stages_list = data[0];
      this.get_pileline_stages(newdata);
      
     /* console.log('pipeline stages list', data);*/
    }, (err)=> {
        if (err.ok == false) {
          this.loader.dismiss();
          // this.get_pileline_stages_data(this.pipeline_uuid);
        }
    });
  }

  // LIST OF PIPELINE
  private pipeline_list;
  private selected_pipeline_add_deal;
  get_pipeline_list(){
    this.pipeline_service.get_pipeline_list_api().subscribe(data=>{
      /*console.log('list of pipeline', data.Items);*/
      this.pipeline_list = data.Items;
      this.selected_pipeline_add_deal = data.Items[0].uuid;
      this.get_stages_for_deal(this.selected_pipeline_add_deal);
      /*this.events.publish('pipeline:stage', data.Items[0].uuid);*/
      // this.get_pileline_stages(data.Items[0].uuid);
    }, error=> console.log('error=', alert(error) ));
  }

  private selected_stage_uuid_for_add_deal;
  get_stages_for_deal(p_uuid){
    this.pipeline_service.pipeline_stages_deals(p_uuid, 'all', 'user_name=').subscribe(data=>{
      this.stages_list = data[0]
      this.selected_stage_uuid_for_add_deal = data[0][0].uuid;
    }, (err)=> {
        if (err.ok == false) {}
    });
  }

  get_pileline_stages(data){
    var stage_uuid = data.stage_uuid;
    this.stages_list.filter((x, indx)=>{ 
      if (x.uuid == stage_uuid) {
        this.stages_index = indx;
      }
    });
    this.navCtrl.push(DealDetailPage,{
      'detail_list_pipeline': data, // single json for deal data
      'list_of_stages': this.stages_list, // json array for stages list
      'list_of_pipeline': this.pipeline_list, // json array for pipeline list
      'selected_pipeline_id': this.navParams.get('selected_pipeline_id'), // pipeline uuid
      'stagesindex': this.stages_index,
    });
  }

  
  
 ionViewDidLoad() {
    /*console.log('ionViewDidLoad PersonDetailPage');*/
  }

  pushOnAddDealEdit(){
    this.navCtrl.push(AddPersonPage, {
      'value': 0,
      'contact_person_detail':this.person_data,
      'contact_organization_detail':this.organizationdata,

    });
  }
}