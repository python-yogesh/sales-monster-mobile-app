import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LostReasonPage } from './lost-reason';

@NgModule({
  declarations: [
    LostReasonPage,
  ],
  imports: [
    IonicPageModule.forChild(LostReasonPage),
  ],
})
export class LostReasonPageModule {}
