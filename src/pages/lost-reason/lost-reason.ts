import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { UserService } from '../../providers/api-services/user.service';

@IonicPage()
@Component({
  selector: 'page-lost-reason',
  templateUrl: 'lost-reason.html',
})
export class LostReasonPage {
  constructor(public navCtrl: NavController, public navParams: NavParams, public authUser:UserService, public events:Events) {
  }

  ionViewDidLoad() {
    /*console.log('ionViewDidLoad LostReasonPage');*/
  }

  lost_deal(lost_reason){
  	if (lost_reason=='') {
  		alert('Choose lost reason');
  		return;
  	}
    var data = {'lost_reason': lost_reason};
    this.authUser.won_lost_api(this.navParams.get('deal_uuid'), data).subscribe(data=>{
      if (data.ResponseMetadata.HTTPStatusCode == 200) {
      	this.events.publish('get_deal_list');
        this.events.publish('person_list:listing');
        this.events.publish('organisation_detail:listing');
        this.navCtrl.pop();
      }
    },(err) => console.error(err) );
  }

}
