import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomFieldPage } from './custom-field';

@NgModule({
  declarations: [
    CustomFieldPage,
  ],
  imports: [
    IonicPageModule.forChild(CustomFieldPage),
  ],
})
export class CustomFieldPageModule {}
