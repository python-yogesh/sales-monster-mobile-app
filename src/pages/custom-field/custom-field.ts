import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,Events } from 'ionic-angular';
import { SharedComponent } from '../../providers/api-services/_sharedComponent';
import { UserService } from '../../providers/api-services/user.service';

/**
 * Generated class for the CustomFieldPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-custom-field',
  templateUrl: 'custom-field.html',
})
export class CustomFieldPage {
  private deal_field;
  private start_date: any;
  private end_date: any;
  private _sharedComponent = new SharedComponent();
  private value;
  constructor(public navCtrl: NavController, public navParams: NavParams,public authUser: UserService,public events: Events,) {
    this.deal_field = this.navParams.get('deal_field');
    this.value = this.navParams.get('value');
    var begin_date = this.deal_field.value.beginJsDate;
    var end_date = this.deal_field.value.endJsDate;
    this.start_date = this._sharedComponent.dateToYMD(new Date(begin_date));
    this.end_date = this._sharedComponent.dateToYMD(new Date(end_date));

  }
  /*get_multipleoption(single_value, selected_value){
    console.log(selected_value)
    for(var j=0; j <= selected_value.length; j++){
      if(single_value == selected_value[j]){
        return true;
      }
    }
  }
  get_singleoption(single_value, selected_value)
  {
     if(single_value == selected_value)
     {
       return true;
     }
  }*/
  update_single_deal_custom_field_value(deal_form_data){
    deal_form_data.value['uuid'] = this.deal_field.deal_uuid;
    var deal_uuid = this.deal_field.deal_uuid;
    this.authUser.update_deal_api(deal_uuid, deal_form_data.value)
    .subscribe(data=>{
      /*this.sendDealEvent.emit();*/
      this.events.publish('get_deal_list');
      alert('update successfully');
    });
  }
  
  private uuid;
 // update single person and org fields.
  update_single_org_custom_field_value(table_name, org_form_data_value){

    // uuid => person_uuid, org_uuid, deal_uuid
    this.uuid = this.deal_field.person_uuid;
    let data = org_form_data_value;
    this.authUser.update_single_field_value_org(table_name, this.uuid, data.value)
      .subscribe(data=>{
        this.events.publish('person_list:listing');
        alert('update successfully');
     });
  }
    

     // update single person and org fields.
  update_org_custom(table_name, org_form_data_value, uuid){
    // uuid => person_uuid, org_uuid, deal_uuid
    let data = org_form_data_value;
    this.uuid = this.deal_field.org_uuid;
    this.authUser.update_single_field_value_org(table_name, this.uuid, data.value)
      .subscribe(data=>{
        this.events.publish('organisation_detail:listing');
        alert('update successfully');
    });
  }
  ionViewDidLoad() {
    /*console.log('ionViewDidLoad CustomFieldPage');*/
  }

}
