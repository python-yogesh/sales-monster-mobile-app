import { Component } from '@angular/core';
import { IonicPage, Events, NavController, NavParams,ToastController } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
// import component
import { SearchPage } from '../search/search';
import { ContactPage } from '../contact/contact';

// import services
import { CognitoUtil } from '../../providers/cognito.service'
import { UserLoginService } from '../../providers/userLogin.service'
import { ContactService, UserService } from '../../providers/api-services/user.service';

@IonicPage()
@Component({
  selector: 'page-add-person',
  templateUrl: 'add-person.html',
})
export class AddPersonPage {
	private myForm: FormGroup;
  private organizationForm: FormGroup;
  private value;
  private current_owner_name;
  private person_uuid;
  private organisation_uuid;
  private person_data = { person_name:'',contact_organization_data:'', contact_organization_name:'',person_phone:'',
    person_email:'',owner_follower:'',owner_id:''
  }

  private org_address;
  org_icon: string = 'add';


  private organisation_data ={contact_organization_name:'',owner_id:'',owner_follower:'',address:''}
  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder,
  						public contact_services: ContactService, public events: Events,
              public auth_user_services: UserService, cognito_service: CognitoUtil,
              public user_login_service: UserLoginService,public toastCtrl: ToastController,) {
  	this.myForm = this.formBuilder.group({
      'person_name': ['', Validators.compose([Validators.required])],
      'contact_organization_data': [''],
      'contact_organization_uuid': [''],
      'organization_name': [''],
      'person_phone': this.formBuilder.array([ this.initPhone() ]),
      'person_email': this.formBuilder.array([ this.initEmail() ]),
      'owner_follower': [true],
      'owner_id': [''],
    });
    // 
    this.organizationForm = this.formBuilder.group({
      'contact_organization_name': [''],
      'owner_id': [''],
      'owner_follower': [true],
      'address': ['']
    });

    // return to search page.
  	events.subscribe('contact:org', (userEventData) => {
      let data = { uuid: userEventData.org_uuid }
      this.myForm.controls['organization_name'].setValue(userEventData.org_name);
      this.myForm.controls['contact_organization_data'].setValue(data);
      this.myForm.controls['contact_organization_uuid'].setValue(userEventData.org_uuid);
      this.org_icon = "close";
    });
    
    var contact_person_detail = this.navParams.get('contact_person_detail');
    var contact_organization_detail = this.navParams.get('contact_organization_detail');
 
    if(contact_person_detail!=undefined){
      this.person_uuid = contact_person_detail.uuid;
      this.form_value = true;
      if(this.form_value == true){
        this.org_icon = 'close';
      }
      this.person_data = contact_person_detail;
      this.person_data.contact_organization_name = contact_organization_detail.contact_organization_name;
      /*let data = { uuid: contact_organization_dethghjghjghjail.uuid };*/
      // this.myForm.controls['contact_organization_data'].setValue(data);
      this.myForm.controls['contact_organization_uuid'].setValue(contact_organization_detail.uuid);
      if (contact_person_detail.person_email!=undefined) {    
          this.person_data.person_email = contact_person_detail.person_email[0].email;
      }
      if (contact_person_detail.person_phone!=undefined) {
          this.person_data.person_phone = contact_person_detail.person_phone[0].phone;
      }
    }
    
    // this.person_data.person_email = contact_person_detail.person_email[0].email;
    this.value = this.navParams.get('value');
    /*console.log(this.value)*/
    this.current_owner_name = this.user_login_service.cUtil.getCurrentUser().username
    this.get_user_list();

    
    if(contact_organization_detail!=undefined){
      this.organisation_uuid = contact_organization_detail.uuid;
      this.form_value = true;
      this.organisation_data = contact_organization_detail;
      this.organisation_data.contact_organization_name = contact_organization_detail.contact_organization_name;
      this.org_address = this.dynamic_address(this.organisation_data.address);
     }      
  }

  dynamic_address(address){
    try{
      var data = JSON.parse(address); 
      if (data['formatted_address']) { 
        return data['formatted_address']; 
      } return address;
    } catch (e) { 
        return address;
      }
  }


  /*organisation page*/
  ionViewDidLoad() {
    /*console.log('ionViewDidLoad AddPersonPage');*/
  }
  
  private form_value;
  ngOnInit(){      
    
  }

  private user_list_data;
  get_user_list(){
    this.auth_user_services.get_user_list_api().subscribe(data=>{
      this.user_list_data = data.users;
    },(err) => alert(err));
  }

  //this is for add field/
	initPhone() {
	  return this.formBuilder.group({
	      phone: [''],
	      selF: ['Work']
	  });
	}
	
	initEmail() {
    return this.formBuilder.group({
        email: [''],
        selF: ['Work']
    });
	}

  save_data(){
		if (this.value==0) { // if true add people
      let data = this.myForm.value;
      if(data.person_name == "" || data.person_name == null)
      {
        alert("Name field required");
        return;
      }
      delete data.organization_name;
      if (data.contact_organization_data == '') {
        data.contact_organization_data = undefined
      }
      if (data.person_email[0].email == '') {
        delete data.person_email
      }
      if (data.person_phone[0].phone == '') {
        delete data.person_phone
      }
        this.contact_services.create_person_api(data).subscribe(data=>{
          if (data.status == 200) {
            if (data.json().personstatus==true){
              alert(data.json().message);
            }else{
              this.events.publish('contact_person:listing');
              alert('Create Person successfully');
              this.navCtrl.pop();
            }
          }
        },(err) => console.error(alert(err)) );
    }
    else{ // if false add organazation
      if (this.organizationForm.valid) {
        let data = this.organizationForm.value;
        if(data.contact_organization_name == "" || data.contact_organization_name == null)
        {
          alert("Organisation field required");
          return;
        }

        this.contact_services.create_contact_organization_api(data).subscribe(data=>{
          if (data.status == 200) {
            if (data.json().organizationstatus==true){
              alert('Organization Already Exist');
            }
            else{
              this.events.publish('contact_org:listing');
              alert('add successfully');
              this.navCtrl.pop();
            }
          }
        },(err) => console.error(err) );
      }else{
        alert('Please add a name to your contact person');
      }
    }
	}
  deletePerson(){
    if (this.value==0){
      this.contact_services.person_delete_api(this.person_uuid).subscribe(data=>{
          alert('Delete Person successfully');
          this.navCtrl.setRoot(ContactPage);
          this.events.publish('contact_person:listing');
        },(err)=>{
      });
    }else{
      this.contact_services.organisation_delete_api(this.organisation_uuid).subscribe(data=>{
          alert('Delete Organisation successfully');
          this.events.publish('contact_org:listing');
          this.navCtrl.setRoot(ContactPage);
        }, (err)=>{
      });
    }
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
    });
    toast.present();
  }

  update_data(){
    if (this.value==0){
      let data = this.myForm.value;
      if(data.person_name == "" || data.person_name == null)
      {
        alert("Name field required");
        return;
      }
      delete data.contact_organization_data;
      delete data.organization_name;
      if (data.person_email[0].email == '') {
        delete data.person_email
      }
      if (data.person_phone[0].phone == '') {
        delete data.person_phone
      }
      /*console.log(data);*/
      if (this.form_value) { // update person
        this.contact_services.person_update_api(this.person_uuid, data).subscribe(data=>{
          if (data.status == 200) {
              this.events.publish('person_list:listing');
              this.events.publish('contact_person:listing');
              alert('Person updated successfully');
              this.navCtrl.pop();
            }
          }, (err)=>{
        });
      }
    }
   else{
       let data=this.organizationForm.value;
       if(data.contact_organization_name == "" || data.contact_organization_name == null)
        {
          alert("Organisation field required");
          return;
        }
      /* console.log(data);*/
       if (this.form_value) {
         this.contact_services.organisation_update_api(this.organisation_uuid, data).subscribe(data=>{
            if (data.ResponseMetadata.HTTPStatusCode == 200) {
                this.events.publish('organisation_detail:listing');
                this.events.publish('contact_org:listing');
                this.presentToast('Organisation update successfully');
              }
            }, (err)=>{
          
        });
      
       }

     }
  }

  person_icon(ioninput){
    if(ioninput.attributes[4].value == "add"){
      this.navCtrl.push(SearchPage, {
      'contact_org_list': this.navParams.get('contact_org_list'),
      'value': 1
    }); 
    }
    else{
       this.person_data.contact_organization_name = "";
       this.org_icon = "add";
    }
  }

  // push on SearchPage when we click on input button
  // navParams get to contact page
  
  contact_org_search_page(input){

    this.navCtrl.push(SearchPage, {
        'contact_org_list': this.navParams.get('contact_org_list'),
        'value': 1
      });
  }
}