import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SharedComponent } from '../../providers/api-services/_sharedComponent';
import { DealService } from '../../providers/api-services/user.service';



declare var $:any;
declare var moment:any;
// declare var toastr:any;


/**
 * Generated class for the StatisticsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-statistics',
  templateUrl: 'statistics.html',
})
export class StatisticsPage {
  private _sharedComponent = new SharedComponent();
  private dealData;
  private wondealData 
  private lostdealData;
  

  user_color = 'This month';
  display_icon = 'This month';

  everyone_color = "everyone";
  everyone_icon = "everyone";

  pipeline_color = 'pipeline';
  pipeline_icon = 'pipeline';

  constructor(public navCtrl: NavController, public navParams: NavParams,private deal_services: DealService) {
     this.date_sidebar('This month');
     this.everyone_sidebar('everyone');
     this.pipeline_sidebar('pipeline');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StatisticsPage');
  }

  ngAfterViewInit(){
    $(document).ready(function () {
        $(".sidebar-dashboard").mCustomScrollbar({
            theme: "minimal"
        });
        $(".sidebar-dashboard").mCustomScrollbar('destroy');
        $('.dismiss-dashboard, .overlay-dashboard').on('click', function () {
            $('.sidebar-dashboard').removeClass('active');
            $('.overlay-dashboard').fadeOut();
        });

        $('.dismiss-sidebar, .overlay-dashboard').on('click', function () {
            $('.sidebar-dashboard').removeClass('active');
            $('.overlay-dashboard').fadeOut();
        });
        
        $('.dismiss_').on('click', function () {
            $('.sidebar-dashboard').removeClass('active');
            $('.overlay-dashboard').fadeOut();
        });

        $('.sidebarCollapse-dashboard').on('click', function () {
            $('.sidebar-dashboard').addClass('active');
            $('.overlay-dashboard').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
  }

  range =  {
      'Yesterday':[moment().subtract(1, 'day'), moment().subtract(1, 'day')],
      'Today':[moment(), moment()],
      'This month':[moment().startOf('month'), moment().endOf('month')],
      'Last month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      'Last 6 Months': [moment().subtract(6, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      'This year': [moment().startOf('year'), moment().endOf('year')],
    }

  get_statistics_deal(dealstatus, start_at, end_at){
    this.deal_services.get_statistics_deal_(dealstatus, start_at, end_at).subscribe(data=>{
       if (dealstatus==1)
         this.dealData = data;
       else if (dealstatus==2)
         this.wondealData = data;
       else if (dealstatus==3)
         this.lostdealData = data;
    });
  }

  sidebar_status(text: string){
    this.user_color = text;
    this.display_icon = text;
  }

  everyone_sidebar_status(text: string){
    this.everyone_color = text;
    this.everyone_icon = text;
  }
  
  everyone_sidebar(text: string){
    this.everyone_sidebar_status(text);
  }
  
  pipeline_sidebar_status(text: string){
    this.pipeline_color = 'pipeline';
    this.pipeline_icon = 'pipeline';
  } 

  pipeline_sidebar(text: string){
    this.pipeline_sidebar_status(text);
  }
  

  date_sidebar(text: string){
    this.sidebar_status(text);
    var date_range = this.range[text];
    var start_at = this._sharedComponent.dateToYMD(new Date(date_range[0]));
    var end_at = this._sharedComponent.dateToYMD(new Date(date_range[1]));
    this.get_statistics_deal(1, start_at, end_at);
    this.get_statistics_deal(2, start_at, end_at);
    this.get_statistics_deal(3, start_at, end_at);
 }

  // calendar
  public mainInput = {
    start: moment().startOf('month'),
    end: moment().endOf('month'),
  }

}