import { Component } from '@angular/core';
import { IonicPage, Events, NavController, NavParams, Nav } from 'ionic-angular';
import { ContactService,DealService } from '../../providers/api-services/user.service';
import { AddPersonPage } from '../add-person/add-person';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
	private person_data;
	private contact_org_list;
  private pipeline_list;
  private value;
  private product_list_data;
  private deal_uuid;
  constructor(public navCtrl: NavController, public navParams: NavParams , public nav: Nav,
  						public events: Events, public contact_services: ContactService,public dealService:DealService) {
    this.pipeline_list = this.navParams.get('pipelinestages');
    this.value = this.navParams.get('value');
    this.deal_uuid = this.navParams.get('uuid');
    if(this.value==0){
      this.get_contact_person_list('All');
    }
    else if(this.value==1)
    {
      this.get_contact_organization_list('All');
    }
    else
    { 
      this.get_product_list();
    }

    events.subscribe('contact_person:listing', () => {
      this.get_contact_person_list("All");
    });
    events.subscribe('contact_org:listing', () => {
      this.get_contact_organization_list("All");
    });
    events.subscribe('product_list:listing', () => {
      this.get_product_list();
    });
  }

  ionViewDidLoad() {
    /*console.log('ionViewDidLoad SearchPage');*/
  }

  // get list of all contact person
  get_contact_person_list(charT){
    this.contact_services.contact_person_list_api(charT).subscribe(data=>{
      if(data.length!=undefined){
        this.person_data = data;
      }
    });
  }

  // get list of all contact person
  get_contact_organization_list(charT){
    this.contact_services.contact_organization_list_api(charT).subscribe(data=>{
      this.contact_org_list = data;
    });
  }
   
  get_product_list(){
    var deal_uuid = this.navParams.get('uuid');
    this.dealService.product_list_api(deal_uuid).subscribe(data=>{
      this.product_list_data = data;
      console.log(data);
    });
  }

  add_to_product(data){
    var product = {"is_product": true, "product_uuid": data.uuid}
    this.dealService.add_product_in_deal_detail(this.deal_uuid, product).subscribe(data=>{
      this.events.publish('get_product_list');
      this.events.publish('get_deal_list');
      this.navCtrl.pop();
    });
  }

  goToBack_person(person_name, person_uuid){
		var data = { person_name: person_name, person_uuid: person_uuid }
    this.navCtrl.pop();
	  this.events.publish('contact:person', data);
  }

  goToBack_org(org_name, org_uuid){
    var data = {org_name: org_name, org_uuid: org_uuid}
		this.navCtrl.pop();
	  this.events.publish('contact:org', data);
  }

  push_on_create_person_page(){
    this.navCtrl.push(AddPersonPage,{
      'value': this.navParams.get('value')
    });
  } 
}