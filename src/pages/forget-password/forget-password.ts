import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,LoadingController } from 'ionic-angular';
import {UserLoginService} from "../../providers/userLogin.service";


@IonicPage()
@Component({
  selector: 'page-forget-password',
  templateUrl: 'forget-password.html',
})
export class ForgetPasswordPage {
  verificationCode: string;
  email: string;
  password: string;
  errorMessage: string;
  showResetPassword: boolean;
  password_type: string = 'password';

  constructor(public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController,public alertCtrl: AlertController,public userService: UserLoginService) {
  }
  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    this.loader.present();
  }

  //call for forgot password
  onNext(){
    if (this.email==null) {
      alert('Enter email id');
      return;
    }else{
      this.presentLoading();
      this.userService.forgotPassword(this.email, this);
    }
  }

  cognitoCallback(message: string, result: any){
    this.loader.dismiss();
    if(message!=null){
      alert(message);
    }else {
      this.showResetPassword = true;
    }
  }

   onConfirmNewPassword(){
    if (this.verificationCode==null || this.password == null){
      alert('Verification Code & Password required');
      return;
    }else{
      this.presentLoading();
      this.userService.confirmNewPassword(this.email, this.verificationCode, this.password, this);
    }
  }
  
  CognitoCallback_confirm_code(message: string, result: string){
    this.loader.dismiss();
    if(message!=null){
      alert(message);
    }else{
      this.navCtrl.pop();
    }
  }

  togglePasswordMode(){
    this.password_type = this.password_type === 'text' ? 'password' : 'text';
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgetPasswordPage');
  }
}