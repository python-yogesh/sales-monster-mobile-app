import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Slides, Nav, Events } from 'ionic-angular';
import { ContactService} from "../../providers/api-services/user.service";
import { AddPersonPage } from '../add-person/add-person';
import { PersonDetailPage } from '../person-detail/person-detail';
import { OrganisationDetailPage } from '../organisation-detail/organisation-detail';
import 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';

declare var $:any;

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {
  @ViewChild('pageSlider') pageSlider: Slides;
  tabs: any = '0';
   selectTab(index) {
     this.pageSlider.slideTo(index);
     
   }
   changeWillSlide($event) {
    this.tabs = $event._snapIndex.toString();
  }
  
  constructor(public navCtrl: NavController, public navParams: NavParams, platform: Platform, 
              public contact_services: ContactService, private nav: Nav,
              public events: Events) {
    events.subscribe('contact_person:listing', () => {
      this.get_contact_person_list("All");
    });
    events.subscribe('contact_org:listing', () => {
      this.contact_organization_list("All");
    });
    this.get_contact_person_list("All");
    this.contact_organization_list("All");
    
  }

  push_on_create_page(){
    this.nav.push(AddPersonPage, {
      'contact_org_list': this.contact_org_list,
      'value': this.tabs,
    });
  }

  ionViewDidLoad() {
    /*console.log('ionViewDidLoad ContactPage');*/
  }

  ionViewDidEnter() {
    /*console.log('contact ionViewDidEnter')*/
  }

  ionViewWillLeave(){
    /*console.log('viewWillLeave');*/
  }

  /*private search_data;
  initialize_fields_data(){
    this.search_data = this.searchobject;
  }*/
  // private search_data;
  initialize_fields_data(){
    this.searchobject = this.custom_field;
  }

  onInput(event){
    this.initialize_fields_data()
    let val = event.target.value;
    if (val && val.trim() != ''){
      if (this.searchobject!=undefined) {
          this.searchobject = this.searchobject.filter((item) => {
          let name: any = item;
          return (name.person_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
      }
    }
  }

  ngAfterViewInit(){
    $(document).ready(function () {
        $(".sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('.dismiss, .overlay').on('click', function () {
            $('.sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('.sidebarCollapse').on('click', function () {
            $('.sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
  }

  doRefresh(refresher){
    // console.log('Begin async operation', refresher);
    setTimeout(() => {
      // console.log('Async operation has ended');
      this.get_contact_person_list("All");
      refresher.complete();
    }, 2000);
  }
  
  private custom_field;
  private searchobject = [];
  private personmessage;
  // get list of all contact person
  get_contact_person_list(charT){
    this.contact_services.contact_person_list_api(charT).subscribe(data=>{
      this.searchobject = [];
        if(data.length!=undefined){
          var person_list = data;
          for (var i = 0; i < person_list.length; i++) {
            this.searchobject.push({
              'person_name': person_list[i].person_name,
              'uuid': person_list[i].uuid,
              'org_name': person_list[i].person_organization,
            });
          }
          // console.log('contact person list', data);
          this.custom_field = this.searchobject;
          this.personmessage = this.searchobject.length;
        }
    });
  }
  
  push_on_persondetail(uuid){
    this.navCtrl.push(PersonDetailPage, {
      'deal_person_uuid': uuid,
    });
  }

  push_on_organisationdetail(data){
    this.navCtrl.push(OrganisationDetailPage, {
      'contact_organisation_uuid': data.uuid,
      
    });
  }
  private contact_org_list=[];
  private orgmessage;
  // get list of all contact person
  contact_organization_list(charT){
    this.contact_services.contact_organization_list_api(charT).subscribe(data=>{
      this.contact_org_list = data;
      this.orgmessage = this.contact_org_list;
      // console.log('contact organization list', data);
    });
  }

}