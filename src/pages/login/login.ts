import { Component } from '@angular/core';
import { NavController, AlertController,LoadingController, Events} from 'ionic-angular';
// import components;
import { SignupPage } from '../signup/signup';
import { ConfirmRegistrationPage } from '../confirm-registration/confirm-registration';
import { ForgetPasswordPage } from '../forget-password/forget-password';
import { TabsPage } from '../tabs/tabs';
// import serbices
import {UserLoginService} from "../../providers/userLogin.service";
import { UserService } from "../../providers/api-services/user.service";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
	email: string;
  password: string;
  show = 'Show'; 
  icon_type = 'visibility';
  password_type: string = 'password';
  constructor(public navCtrl: NavController, public userService: UserLoginService, public alertCtrl: AlertController,public loadingCtrl: LoadingController, public auth_user_sevices: UserService, 
    public event: Events) {
    /*console.log('login component');*/
  }

  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    this.loader.present();
  }
  
  signup_page(){
  	this.navCtrl.setRoot(SignupPage);
  }

  loginMe(){
    if (this.email == null || this.password == null) {
        this.doAlert("Error", "All fields are required");
        return;
    }
    this.presentLoading();
    this.userService.authenticate(this.email, this.password, this);
  }

  cognitoCallback_login(message: string, result: any) {
    if (message != null) { //error
        this.loader.dismiss();
        this.doAlert("Error", message);
        if (message === 'User is not confirmed.') {
          this.navCtrl.push(ConfirmRegistrationPage, {
            'email': this.email,
            'password': this.password
          });
        }
    }else{ //success
      this.loader.dismiss();
      this.navCtrl.setRoot(TabsPage);
    }
  }
  togglePasswordMode() {   
    this.password_type = this.password_type === 'text' ? 'password' : 'text';
  }

  doAlert(title: string, message: string) {
    let alert = this.alertCtrl.create({
        title: title,
        subTitle: message,
        buttons: ['OK']
    });
    alert.present();
  }

  forget_password(){
    this.navCtrl.push(ForgetPasswordPage);
  }
}