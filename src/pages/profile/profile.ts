import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, Events } from 'ionic-angular';
// import components;
import { LoginPage } from '../login/login';
// import services;
import { UserLoginService } from "../../providers/userLogin.service";
import { UserParametersService } from "../../providers/userParameters.service";
import { UserService } from "../../providers/api-services/user.service";
import { ChangePasswordPage } from '../change-password/change-password';
import { StatisticsPage } from '../statistics/statistics';


@IonicPage()
@Component({
  selector: 'page-profile', 
  templateUrl: 'profile.html',
})
export class ProfilePage {
	private img_url
  private user_object = {};
  private cognito_user_name: string;
  private company_name;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
              public userService: UserLoginService, public _app: App , 
              private auth_user_services: UserService,private user_parameter: UserParametersService,
              public event: Events,) {
      this.user_parameter.getParameters(this);
      this.get_userProfile();
      this.get_organization();
      // this.user_parameter.getParameters(this);
  }

  callback(){}
  callbackWithParam(result: any){
    for (var i = 0; i < result.length; i++) {
      this.user_object[result[i].Name] = result[i].Value;
    }
    /*console.log('cognito user', this.user_object);*/
    this.cognito_user_name = this.user_object["nickname"];
  }
  
  ionViewDidEnter() {
    this.img_url = 'assets/imgs/profile_img.jpg';
  }
  
  ionViewDidLoad() {
    /*console.log('ionViewDidLoad HomePage');*/
  }

  onLogout(){
    this._app.getRootNav().setRoot(LoginPage);
    this.userService.logout();
  }

  statistics(){
    this.navCtrl.push(StatisticsPage);
  }


  get_organization()
  {
     this.auth_user_services.get_organization().subscribe(data=>{
       this.company_name = data.response_organizations.Item.company_name;

       },(err) => alert(err));
  }

  change_password(){
    this.navCtrl.push(ChangePasswordPage);
  }

  get_userProfile(){ 
    this.auth_user_services.get_user_profile_api().subscribe(data=>{ 
     /* console.log('user profile data', data); */
      if (data.userprofile!='') 
        { 
          var profile_data = data.userprofile; 
            if (profile_data.image_url == "removed")
              { 
                this.img_url = 'assets/imgs/profile_img.jpg'; 
              }
            else
              { 
                this.img_url = profile_data.image_url || 'assets/imgs/profile_img.jpg';
              } 
            } 
    }); 
  }
}