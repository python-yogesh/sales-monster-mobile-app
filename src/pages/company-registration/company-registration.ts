import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,LoadingController} from 'ionic-angular';
//import component
import { TabsPage } from '../tabs/tabs';
//import services
import { UserLoginService } from '../../providers/userLogin.service';
import { CompanyService, StripeServices } from "../../providers/api-services/user.service";

@IonicPage()
@Component({
  selector: 'page-company-registration',
  templateUrl: 'company-registration.html',
})
export class CompanyRegistrationPage {
	private submite_disabled = true;
	private validation_error;
  private company_loader_img = true;
	company_name: string;
	employee_size: string;
	category: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
  						public company_services: CompanyService, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public user_login_service: UserLoginService,
              public stripe_services: StripeServices) {
    /*console.log('company component');*/
    this.user_login_service.cUtil.getCurrentUser().username;
  }

  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    this.loader.present();
  }

  ionViewDidLoad() {
    /*console.log('ionViewDidLoad CompanyRegistrationPage');*/
  }

  focusOutFunction(company_name){
  	var data = {'companyName': company_name.target.value}
    this.company_services.company_name_validation_api(data).subscribe(data=>{
      if (data) {
        this.submite_disabled = false;
        this.validation_error = false;
      }else{
        this.submite_disabled = true;
        this.validation_error = true;
      }
    });
  }

  employees = [
    {value: '10', employee:'1 to 10'},
    {value: '25', employee:'upto 25'},
    {value: '50', employee:'upto 50'},
    {value: '100', employee:'upto 100'},
    {value: '101', employee:'more than 100'},
  ]

  Organization = [
    {value: 'it_services', name:'IT Services'},
    {value: 'software', name:'Software'},
    {value: 'banking', name:'Banking'},
    {value: 'other', name:'Other'},
  ]

  navigateOnTabs(){
    setTimeout(() => {
      this.navCtrl.setRoot(TabsPage);
    }, 15000);
  }

  create_company(){
    var data = { 'company_name': this.company_name,  'employee_size': this.employee_size, 'category': this.category }
    if (this.company_name!=null && this.employee_size!=null && this.category!=null) {
        this.presentLoading();
		    this.company_services.create_company_(data).subscribe(data=>{
		      /*console.log('create company response:', data.json());*/
		      if (data.status == 200) {
              this.loader.dismiss();
              this.navigateOnTabs();
              this.company_loader_img = false;
              var dd = data.json();
              // var obj = dd.organization_json.Item
              this.create_stripe_trail(dd.organization_json.Item);
		      }
		    },(err)=>{
          alert(err.status)
        });
    }else{
    	this.doAlert('All fields required');
    }
  }

  create_stripe_trail(data){
    var obj = { 'username': this.user_login_service.cUtil.getCurrentUser().username, 
      "organization_uuid" : data.uuid, "organization_name" : data.company_name
    }
    this.stripe_services.create_trail_stripe_api(obj).subscribe(data=>{
      if (data.status_code==200){
        // alert('success ');
        // localStorage.setItem('customer_id', data.customer)
      }
    },(err)=>{
      alert(err);
    });
  }


  doAlert(message: string) {
    let alert = this.alertCtrl.create({
        subTitle: message,
        buttons: ['OK']
    });
    alert.present();
  }
}