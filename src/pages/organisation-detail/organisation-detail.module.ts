import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrganisationDetailPage } from './organisation-detail';

@NgModule({
  declarations: [
    OrganisationDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(OrganisationDetailPage),
  ],
})
export class OrganisationDetailPageModule {}
