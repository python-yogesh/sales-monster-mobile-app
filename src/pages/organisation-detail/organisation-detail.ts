import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events,LoadingController } from 'ionic-angular';
import { ContactService } from '../../providers/api-services/user.service';
import { AddPersonPage } from '../add-person/add-person';
import { AddActivityPage } from '../add-activity/add-activity';

import { DealDetailPage } from '../deal-detail/deal-detail';
import { CustomFieldPage } from '../custom-field/custom-field';


import { AddDealPage } from '../add-deal/add-deal';
import { PipelineService,UserService } from "../../providers/api-services/user.service";



@IonicPage()
@Component({
  selector: 'page-organisation-detail',
  templateUrl: 'organisation-detail.html',
})
export class OrganisationDetailPage {
	private contact_person_detail;
	private organisation_uuid;
	private organisation_data;
	private person_data;
	private deals;
	private won;
	private lost;
	private open;
	private open_length;
	private won_length;
	private lost_length;
	private detail_list;
	private pipeline_stages;
  private stages_index;
  private currency_data;
  private stages_list;
  private organisation_single_data;

  constructor(public navCtrl: NavController, public navParams: NavParams,public contactservice: ContactService,public events: Events,public pipeline_service: PipelineService,public authUser: UserService,public loadingCtrl: LoadingController) {
    this.contact_person_detail  = this.navParams.get('contact_person_detail');
    this.organisation_uuid = this.navParams.get('contact_organisation_uuid');
    var contact_organization_detail = this.navParams.get('contact_organization_detail');
    if(contact_organization_detail!=undefined){
      this.organisation_uuid = contact_organization_detail.uuid;
    }

    this.detail_list = this.navParams.get('detail_list');
    this.pipeline_stages = this.navParams.get('pipeline_stages');
    this.stages_index = this.navParams.get('stagesindex');
    

    this.get_organisation();
    this.deals = "general";
    events.subscribe('organisation_detail:listing', () => {
      this.get_organisation();
    });

    this.get_pipeline_list();
  }

  ionViewDidLoad() {
    /*console.log('ionViewDidLoad OrganisationDetailPage');*/
  }

  dynamic_address(address){
    try{
      var data = JSON.parse(address); 
      if (data['formatted_address']) { 
        return data['formatted_address']; 
      } return address;
    } catch (e) { 
        return address;
      }
  }

  private org_address;
  get_organisation(){
  	this.contactservice.organisation_get_api(this.organisation_uuid).subscribe(data=>{
  		// console.log('organisation_data', data);
      this.org_address = this.dynamic_address(data['contactOrganization'].address);
      this.organisation_data = data['contactOrganization'];
      this.person_data = data.contactPersons;
      this.organisation_single_data = data.contactOrganization;
      var organisation_deals = data.deals;
      this.won = organisation_deals.filter((x)=>{ return x.dealstatus==2 });
      this.lost = organisation_deals.filter((y)=>{ return y.dealstatus==3 });
      this.open = organisation_deals.filter((z)=>{ return z.dealstatus==1 });
      this.open_length = this.open.length;
      this.won_length = this.won.length;
      this.lost_length = this.lost.length;
      this.currency_data = data.currency_data;
      
      this.list_of_fields(this.organisation_data);

    });
  }
  
  isBigEnough(element, index, array) {
    return (element.is_deleted==true); 
  }

  private contacts_organizations_fields_obj; // form variable
  private contacts_organizations_fields;
  private deal;
  private contacts_organizations_length;
  list_of_fields(deal_obj){
    this.deal = deal_obj;
    this.authUser.get_list_custom_field('True').subscribe(data=>{
      // console.log('custom fields', data)
      // org custom fields data
      this.contacts_organizations_fields = data.contacts_organizations_fields.filter(this.isBigEnough);
      var contacts_organizations_fields = data.contacts_organizations_fields.filter(this.isBigEnough);
      var contact_org_arr = [];
      for (var j in contacts_organizations_fields){
        var value = deal_obj[contacts_organizations_fields[j].field_api_key] != undefined ? deal_obj[contacts_organizations_fields[j].field_api_key] : '';
        let obj = {
          'field_api_key': contacts_organizations_fields[j].field_api_key,
          'is_type': contacts_organizations_fields[j].is_type,
          'label': contacts_organizations_fields[j].label,
          'multiple_value': contacts_organizations_fields[j].multiple_value,
          'value': value,
          'org_uuid': deal_obj.uuid
        }
        contact_org_arr.push(obj);
      }
      // console.log('contact_org_arr', contact_org_arr)
      this.contacts_organizations_fields_obj = contact_org_arr;
      this.contacts_organizations_length = contact_org_arr.length;
    });
  }

  custom_fields(deal_field){
    this.navCtrl.push(CustomFieldPage, {
      'deal_field':deal_field,
      'value':2,
    });
  }

   push_on_activity_page(){
    this.navCtrl.push(AddActivityPage, {
      'detail_list': this.detail_list,
      'pipeline_stages': this.pipeline_stages,
    });
  }
 
 /*open_detail_page(data){
    this.navCtrl.push(DealDetailPage,{
      'detail_list_pipeline': data, // single json for deal data
      'list_of_stages': this.navParams.get('list_of_stages'), // json array for stages list
      'list_of_pipeline': this.navParams.get('list_of_pipeline'), // json array for pipeline list
      'selected_pipeline_id': this.navParams.get('selected_pipeline_id'), // pipeline uuid
      'pipeline_stages':this.navParams.get('list_of_stages'),
      'stagesindex': this.stages_index,
    });
  }*/

  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    this.loader.present();
  }
  
  private pipelineuuid;
  open_detail_page(newdata){
    this.pipelineuuid = newdata.pipeline_uuid;
    this.presentLoading();
    this.pipeline_service.pipeline_stages_deals(this.pipelineuuid, 'all', 'user_name=').subscribe(data=>{
      this.loader.dismiss();
      this.stages_list = data[0];
      this.get_pileline_stages(newdata);
      /*console.log('pipeline stages list', data);*/
      
    }, (err)=> {
        if (err.ok == false) {
          this.loader.dismiss();
          // this.get_pileline_stages_data(this.pipeline_uuid);
        }
    });
  }

  private selected_stage_uuid_for_add_deal;
  get_stages_for_deal(p_uuid){
    this.pipeline_service.pipeline_stages_deals(p_uuid, 'all', 'user_name=').subscribe(data=>{
      this.stages_list = data[0]
      this.selected_stage_uuid_for_add_deal = data[0][0].uuid;
    }, (err)=> {
        if (err.ok == false) {}
    });
  }

  // LIST OF PIPELINE
  private pipeline_list;
  private selected_pipeline_add_deal;
  get_pipeline_list(){
      this.pipeline_service.get_pipeline_list_api().subscribe(data=>{
        /*console.log('list of pipeline', data.Items);*/
        this.pipeline_list = data.Items;
        this.selected_pipeline_add_deal = data.Items[0].uuid;
        this.get_stages_for_deal(this.selected_pipeline_add_deal);
        /*this.events.publish('pipeline:stage', data.Items[0].uuid);*/
        // this.get_pileline_stages(data.Items[0].uuid);
      }, error=> console.log('error=', alert(error) ));
  }
  get_pileline_stages(data){
    var stage_uuid = data.stage_uuid;
    this.stages_list.filter((x, indx)=>{ 
      if (x.uuid == stage_uuid) {
        this.stages_index = indx;
      }
    });
    this.navCtrl.push(DealDetailPage,{
      'detail_list_pipeline': data, // single json for deal data
      'list_of_stages': this.stages_list, // json array for stages list
      'list_of_pipeline': this.pipeline_list, // json array for pipeline list
      'selected_pipeline_id': this.navParams.get('selected_pipeline_id'), // pipeline uuid
      'stagesindex': this.stages_index,
    });
  }
  push_on_adddeal_page(){
    this.navCtrl.push(AddDealPage,{
      'list_of_pipeline': this.pipeline_list,
      'list_of_stages': this.stages_list,
      'selected_pipeline_id':this.selected_pipeline_add_deal,
      'selected_stage_uuid': this.selected_stage_uuid_for_add_deal,
    });
  }
  pushOnOrganisationEdit(){
  	this.navCtrl.push(AddPersonPage, {
      'value': 1,
      'contact_organization_detail': this.organisation_single_data,
    });
  }
}
