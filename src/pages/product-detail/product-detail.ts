import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import { AddProductPage } from '../add-product/add-product';
import { UserService } from '../../providers/api-services/user.service';
import { SearchPage } from '../search/search';

@IonicPage()
@Component({
  selector: 'page-product-detail',
  templateUrl: 'product-detail.html',
})
export class ProductDetailPage {
  private single_deal_obj;
  private product_detail;
  private uuid;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events:Events,public authUser: UserService) {
      /*this.total = this.single_deal_obj.deal_value;
      this.product_detail = this.single_deal_obj.deal_product_response;*/
      this.get_pipeline_list();
      events.subscribe('get_product_list',()=>{
        this.get_pipeline_list();
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductDetailPage');
  }
  get_pipeline_list(){
      var deal_uuid = this.navParams.get('deal_uuid');
      console.log('deal_uuid',deal_uuid);
      this.authUser.get_deal(deal_uuid).subscribe(data=>{ 
        this.single_deal_obj = data.json()['Item'];
        this.product_detail = this.single_deal_obj.deal_product_response;
        this.uuid = this.single_deal_obj.uuid;
        /*console.log('DEAL DETAILS',this.deal);*/
      }, error=> console.log('error=',error));
  }
  product_edit(product_detail){
    this.navCtrl.push(AddProductPage, {
      'single_deal_obj': product_detail,
      'deal_uuid': this.navParams.get('deal_uuid'),
      'value': 1,
    });
  }
  push_on_product_list(){
    this.navCtrl.push(SearchPage, {
      'value': 2,
      'uuid':this.uuid,
    });
  }

}

