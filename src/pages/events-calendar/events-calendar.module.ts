import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventsCalendarPage } from './events-calendar';

@NgModule({
  declarations: [
    EventsCalendarPage,
  ],
  imports: [
    IonicPageModule.forChild(EventsCalendarPage),
  ],
})
export class EventsCalendarPageModule {}
