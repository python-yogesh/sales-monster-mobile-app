import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { mobiscroll } from '@mobiscroll/angular-trial';
import { AddActivityService } from '../../providers/api-services/user.service';


/**
 * Generated class for the EventsCalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var now = new Date();

mobiscroll.settings = {
  theme: 'ios',
  display: 'bottom'
};

@IonicPage()
@Component({
  selector: 'page-events-calendar',
  templateUrl: 'events-calendar.html',
})
export class EventsCalendarPage {
  private displaydate;

  constructor(public navCtrl: NavController, public navParams: NavParams,public activity_services: AddActivityService,) {

    this.get_activity_list();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventsCalendarPage');
  }

  // eventSettings: any = {
  //     theme: 'ios',
  //     display: 'center',
  //     layout: 'liquid',
  //     firstDay: 1,
  //     yearChange: true,
  //     calendarScroll: 'horizontal',
  //     showOuterDays: true,
  //     showEventCount: true
  // };

  // formSettings: any = {
  //     theme: 'ios'
  // };

  /* Day Events */
  calendarSettings: any = {
        theme: 'ios',
        display: 'inline',
        weeks: 1,
        eventBubble: true,
        onDayChange: (day, inst) => {
            if (!day.selected) {
                this.displayEvents(day.date, day.marked);
            }
        },
        onInit: (event, inst) => {
            setTimeout(() => {
                // Display events for today
                this.displayEvents(now, inst.getEvents(now));
            });
        }
    }

    listviewSettings: any = {
        theme: 'ios',
        animation: true,
        swipe: false
    }

    get_activity_list(){
    this.activity_services.get_activities_listing_api().subscribe(data=>{
      if(data!=""){
        this.displaydate = new Date(data[0].start_at);
      }
      this.get_activity_events(data);
    });
    }
    
    private events:any = [];
    get_activity_events(data){
      // this.events = [];
      for (var i =0; i < data.length; i++) {
        var obj = data[i];
        this.addEvent_activity(obj);
      }
     }
     

     addEvent_activity(finaleventdata){
       this.events.push({
         d: new Date(finaleventdata.activity_date),
         text: finaleventdata.activity_start_time + " " + "to" + " "+finaleventdata.activity_end_time + " " + finaleventdata.activityTitle,
         color: '#e7b300'
       });
     }

    selectedDate: Date;

    displayedEvents = [];

    getEventHours(event) {
      return 'Today, ';
    }

    displayEvents(d, events) {
        var displayedEvents = [];
        if (events && events.length) {
            events.forEach(function (event) {
                displayedEvents.push(event);
            });
        } else {
            displayedEvents.push({
                text: 'No events'
            });
        }
        this.displayedEvents = displayedEvents;
        this.selectedDate = d;
    };

     eventSettings: any = {
        theme: 'ios',
        display: 'center',
        layout: 'liquid',
        firstDay: 1,
        yearChange: true,
        calendarScroll: 'horizontal',
        showOuterDays: true,
        showEventCount: true,
    };

    formSettings: any = {
        theme: 'ios'
    };
  }
