import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import { DealService } from '../../providers/api-services/user.service';


@IonicPage()
@Component({
  selector: 'page-add-product',
  templateUrl: 'add-product.html',
})
export class AddProductPage {
  private product_object;
  private form_value;
  private value;
  constructor(public navCtrl: NavController, public navParams: NavParams,public dealService:DealService,public events: Events,) {
    this.product_object = this.navParams.get('single_deal_obj');
    this.value = this.navParams.get('value');
    this.form_value = true;
  }
  update_product(){
    console.log(this.product_object);
    var product_data = this.product_object;
    product_data.qty = JSON.parse(product_data.qty);
    product_data.price = JSON.parse(product_data.price);
    product_data.discount = JSON.parse(product_data.discount);
    product_data.sum = JSON.parse(product_data.sum);
    this.dealService.update_product_deal_detail_api(this.product_object.uuid, product_data).subscribe(data=>{
      if (data.ResponseMetadata.HTTPStatusCode == 200) {
        alert('update successfully');
        this.events.publish('get_product_list');
        this.events.publish('get_deal_list');
        this.events.publish('attach_product');
        this.navCtrl.pop();
      }
    });
  }
  
  save_product(){
    var product_data = this.product_object;
    this.dealService.add_product_in_deal_detail(this.product_object.uuid,product_data).subscribe(data=>{
      if (data.ResponseMetadata.HTTPStatusCode == 200) {
        alert('Create Product Successfully');
        this.events.publish('product_list:listing');
      }
    },(err) => console.error(alert(err)) );
  }

  delete_product(){
    var deal_uuid =  this.navParams.get('deal_uuid');
    this.dealService.delete_product_deal(deal_uuid ,this.product_object.uuid, this.product_object).subscribe(data=>{
      if (data.ResponseMetadata.HTTPStatusCode == 200) {
        alert('Delete successfully');
        this.events.publish('get_product_list');
        this.events.publish('get_deal_list');
        this.navCtrl.pop();
      }
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AddProductPage');
  }

}
