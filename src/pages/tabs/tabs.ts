import { Component, ViewChild } from '@angular/core';
import { IonicPage, MenuController, NavController, NavParams, Tabs } from 'ionic-angular';
import { EventsCalendarPage } from '../events-calendar/events-calendar';
import { ContactPage } from '../contact/contact';
import { ProfilePage } from '../profile/profile';
import { DashboardPage } from '../dashboard/dashboard';
declare var $: any;
@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
	tab1Root = DashboardPage;
	tab2Root = EventsCalendarPage;
  tab3Root = ContactPage;
  tab5Root = ProfilePage;
  @ViewChild('tabsPage') tabRef: Tabs;
  constructor(public navCtrl: NavController, public navParams: NavParams, public menu: MenuController,){
    /*console.log('tabs component');*/
  }

  ionViewDidLoad() {
    // this.menu.enable(true);
    /*console.log('ionViewDidLoad TabsPage');*/
  }
  myMethod(){
    $('.ios .scroll-bar .scroll-content').css('margin-top', '100px');
    $('.md .scroll-bar .scroll-content').css('margin-top', '100px');
  }
}