import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,LoadingController } from 'ionic-angular';
// import components;
import { ConfirmRegistrationPage } from '../confirm-registration/confirm-registration';
import { LoginPage } from '../login/login';

// import services
import { UserRegistrationService } from "../../providers/userRegistration.service";
import { UserService } from "../../providers/api-services/user.service";

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})

export class SignupPage {
  name: string;
  email: string;
  password: string;
  password_type: string = 'password';
  confirm_password_type: string = 'password';
  confirm_password: string;
  confirmpassword: boolean;
  //private tabBarElement;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
              public userRegistration: UserRegistrationService, public auth_user_service: UserService, 
              public alertCtrl: AlertController,public loadingCtrl: LoadingController) {
      /*console.log('signup component');*/
  }
  
  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loader.present();
  }

  ionViewDidLoad() {
   /* console.log('ionViewDidLoad SignupPage');*/
  }

  login_page(){
  	this.navCtrl.setRoot(LoginPage);
  }

  onRegister(){
    this.presentLoading();
    let registrationUser = { email: this.email, password: this.password, name: this.name, confirm_password:this.confirm_password }
    if(this.password == this.confirm_password){
      this.confirmpassword = false;
      this.userRegistration.register(registrationUser, this);
      return;
    }else{
      this.loader.dismiss();
      this.confirmpassword = true;
      return;
    }
  }

  // cognitoCallback work on when we signup new user
  cognitoCallback(message: string, result: any) {
    if (message != null) { //error
        this.loader.dismiss();
        this.doAlert("Registration", message);
    } else { //success
        this.loader.dismiss();
        /*this.create_group_admin();*/
        this.navCtrl.push(ConfirmRegistrationPage, {
            'email': this.email,
            'password': this.password
        });
    }
  }

  togglePasswordMode() {   
   this.password_type = this.password_type === 'text' ? 'password' : 'text';
 }

 confirmPasswordToggle(){
    this.confirm_password_type = this.confirm_password_type === 'text' ? 'password' : 'text';
  }

  // create group admin, when we signup new user
  create_group_admin(){
    this.auth_user_service.add_user_group_(this.email).subscribe(data=>{
      /*console.log('create admin cognito response', data);*/
    });
  }

 doAlert(title: string, message: string) {
    let alert = this.alertCtrl.create({
        title: title,
        subTitle: message,
        buttons: ['OK']
    });
    alert.present();
  }
}