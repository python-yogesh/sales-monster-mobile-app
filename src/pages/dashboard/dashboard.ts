import { Component ,ViewChild} from '@angular/core';
import { NavController ,Slides, Events} from 'ionic-angular';
// import component
import {AddDealPage} from '../add-deal/add-deal';
import { CompanyRegistrationPage } from '../company-registration/company-registration';
import { DealDetailPage } from '../deal-detail/deal-detail';
// import services;
// import { UserLoginService } from "../../providers/userLogin.service";
import { CompanyService, PipelineService, UserService } from "../../providers/api-services/user.service";
declare var $: any;
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {
 @ViewChild('SwipedTabsSlider') SwipedTabsSlider: Slides ;
  SwipedTabsIndicator :any= null;
  tabs:any=[];
  private pipeline_list;
  private image_value: boolean;
  constructor(public navCtrl: NavController, public events: Events, public company_services: CompanyService, 
              public pipeline_service: PipelineService, public user_services: UserService) {
    this.get_pipeline_list();
    this.get_organization();
    this.get_user_list();
    events.subscribe('pipeline:stage',(pipeline_uuid)=>{
      this.selected_pipeline = pipeline_uuid;
      this.get_pileline_stages(pipeline_uuid);
    });
    // this.tabs=["page1 ddd ffff","page2","page3","page4","page5", "page6" , "page7", "page8"];
  }
  ionViewDidEnter(){
    this.SwipedTabsIndicator = document.getElementById("indicator");
    this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.SwipedTabsSlider.getActiveIndex() * 100)+'%,0,0)';
  }

  ionViewDidLeave(){}

  selectTab(index) {
    this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(100*index)+'%,0,0)';
    this.SwipedTabsSlider.slideTo(index, 100);
  }

  updateIndicatorPosition() {
    // this condition is to avoid passing to incorrect index
    if( this.SwipedTabsSlider.length()> this.SwipedTabsSlider.getActiveIndex())
    {
      this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.SwipedTabsSlider.getActiveIndex() * 100)+'%,0,0)';
    }
  }

  changeWillSlide($event){
    // console.log($event.getActiveIndex());
    if ($event.getActiveIndex()<this.tabs.length) {
      this.stages_value = this.tabs[$event.getActiveIndex()];
      // console.log(this.stages_value);
    }
  }

  animateIndicator($event) {
    if(this.SwipedTabsIndicator)
         this.SwipedTabsIndicator.style.webkitTransform = 'translate3d(' + (($event.progress* (this.SwipedTabsSlider.length()-1))*100) + '%,0,0)';
  }

  ngAfterViewInit(){
    $(document).ready(function () {
        $('#indicator').css('display','none');
        $(".sidebar-dashboard").mCustomScrollbar({
            theme: "minimal"
        });
        $(".sidebar-dashboard").mCustomScrollbar('destroy');
        $('.dismiss-dashboard, .overlay-dashboard').on('click', function () {
            $('.sidebar-dashboard').removeClass('active');
            $('.overlay-dashboard').fadeOut();
        });

        $('.dismiss-sidebar, .overlay-dashboard').on('click', function () {
            $('.sidebar-dashboard').removeClass('active');
            $('.overlay-dashboard').fadeOut();
        });
        
        $('.dismiss_').on('click', function () {
            $('.sidebar-dashboard').removeClass('active');
            $('.overlay-dashboard').fadeOut();
        });

        $('.sidebarCollapse-dashboard').on('click', function () {
            $('.sidebar-dashboard').addClass('active');
            $('.overlay-dashboard').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
  }

  private user_color = 'everyone';
  private display_icon = 'everyone';
  sidebar_status(text: string){
    this.user_color = text;
    this.display_icon = text;
  }

  reset_sidebar(text: string){
    this.sidebar_status(text);
  }

  doRefresh(refresher){
    // console.log('Begin async operation', refresher);
    setTimeout(() => {
      // console.log('Async operation has ended');
      this.get_pileline_stages(this.selected_pipeline);
      refresher.complete();
    }, 2000);
  }

  private user_list_data;
  // get users listing display in sidebar
  get_user_list(){
    this.user_services.get_user_list_api().subscribe(data=>{
      this.user_list_data = data.users;
    });
  }

  // LIST OF PIPELINE
  get_pipeline_list(){
    this.pipeline_service.get_pipeline_list_api().subscribe(data=>{
      this.selected_pipeline = data.Items[0].uuid;
      this.pipeline_list = data.Items;
      this.get_pileline_stages(data.Items[0].uuid);
    }, error=> console.log('error=', alert(error) ));
  }

  // partial method
  get_stages_deal_listing(value, filter_value, filter_by){
    this.image_value = true;
    this.pipeline_service.pipeline_stages_deals(value, filter_value, filter_by).subscribe(data=>{
      if(data[0].length!=0){
        this.selected_stage_uuid = data[0][0]['uuid'];
      }
      this.image_value = false;
      $('#indicator').css('display','block');
      // console.log('pipeline stages list', data);
      this.stages_list = data[0];
      this.set_currency = data[2].current_currency;
      this.tabs = [];
      for (var i = 0; i < this.stages_list.length; i++) {
          this.tabs.push({
            'stage': this.stages_list[i].stage,
            'stage_currency_total': this.stages_list[i].stage_currency_total + ' - ',
            'stage_deal_count':  this.stages_list[i].stage_deal_count + ' deal',
            'uuid': this.stages_list[i].uuid,
          });
      }
      // if (this.SwipedTabsSlider.getActiveIndex() < this.tabs.length-1) {
      //   this.stages_value = this.tabs[0];
      //   this.SwipedTabsSlider.slideTo(0, 100);
      // }else{
      //   this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(100*0)+'%,0,0)';
      //   this.SwipedTabsSlider.slideTo(0, 100);
      //   this.stages_value = this.tabs[0];
      // }
      this.stages_value = this.tabs[this.SwipedTabsSlider.getActiveIndex()];
      this.SwipedTabsSlider.slideTo(this.SwipedTabsSlider.getActiveIndex(), 100);
      this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.SwipedTabsSlider.getActiveIndex() * 100)+'%,0,0)';
    }, (err)=> {
        this.image_value = false;
        if (err.ok == false) {
          // this.get_pileline_stages_data(this.pipeline_uuid);
        }
    });
  }

  private stages_list;
  private stages_value = { 'stage': '', 'stage_currency_total': '', 'stage_deal_count':  '', 'uuid': '', };
  private set_currency = '';
  private selected_pipeline;
  private selected_stage_uuid;
  get_pileline_stages(pipeline_uuid){
    this.get_stages_deal_listing(pipeline_uuid, 'all', 'user_name=');
  }

  change_pileline_stages(pipeline_uuid){
    this.SwipedTabsSlider._activeIndex = 0;
    this.selected_pipeline = pipeline_uuid;
    // this.SwipedTabsSlider.slideTo(0, 100);
    this.get_stages_deal_listing(pipeline_uuid, 'all', 'user_name=');
  }

  get_stages_data_by_users_filter(user_obj){
    this.get_stages_deal_listing(this.selected_pipeline, user_obj, 'user_name=');
  }

  filter_deal_data(value){
    this.get_stages_deal_listing(this.selected_pipeline, value, 'filter=');
  }

  get_organization(){
    this.company_services.get_organization_().subscribe(data=>{
      var obj = data.json()
      if (!obj.organization){ // return false don't have a org
        this.navCtrl.setRoot(CompanyRegistrationPage);
      }
    });
  }

  pushOnAddDeal(){
    this.navCtrl.push(AddDealPage,{
      'list_of_pipeline': this.pipeline_list,
      'list_of_stages': this.stages_list,
      'selected_pipeline_id': this.selected_pipeline,
      'selected_stage_uuid': this.selected_stage_uuid,
    });
    // this._app.getRootNav().push(AddDealPage);
  }

  open_detail_page(data, i){
    this.navCtrl.push(DealDetailPage,{
      'list_of_stages': this.stages_list, // json array for stages list
      'list_of_pipeline': this.pipeline_list, // json array for pipeline list
      'selected_pipeline_id': this.selected_pipeline, // pipeline uuid
      'detail_list_pipeline': data, // single json for deal data
      'stagesindex': i // stage index
    });
  }
}