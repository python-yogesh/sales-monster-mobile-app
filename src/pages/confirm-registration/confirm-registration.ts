import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,LoadingController} from 'ionic-angular';
// import components;
import { CompanyRegistrationPage } from "../company-registration/company-registration";
// import services;
import { UserLoginService } from "../../providers/userLogin.service";
import { UserRegistrationService } from "../../providers/userRegistration.service";
import { UserService } from "../../providers/api-services/user.service";

@IonicPage()
@Component({
  selector: 'page-confirm-registration',
  templateUrl: 'confirm-registration.html',
})
export class ConfirmRegistrationPage {
	confirmationCode: string;
  loader;
  constructor(public navCtrl: NavController, public navParams: NavParams, public userRegistration: UserRegistrationService, 
  						public alertCtrl: AlertController, public userService: UserLoginService,
              public loadingCtrl: LoadingController, public user_services: UserService) {
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    this.loader.present();
  }

  ionViewDidLoad() {
    /*console.log('ionViewDidLoad ConfirmRegistrationPage');*/
  }

  onConfirmRegistration() {
    this.presentLoading()
    this.userRegistration.confirmRegistration(this.navParams.get("email"), this.confirmationCode, this);
  }

  // onConfirmRegistration call_back
  cognitoCallback(message: string, result: any) {
      if (message != null) { //error
          this.loader.dismiss();
          this.doAlert("Confirmation", message);
      } else { //success
          let email = this.navParams.get("email");
          let password = this.navParams.get("password");
          if (email != null){
             this.userService.authenticate(email, password, this);
          }
      }
  }

  cognitoCallback_login(message: string, result: any) {
    this.loader.dismiss();
    if (message != null) { //error
      this.doAlert("Error", message);
    }else { //success
    	this.navCtrl.setRoot(CompanyRegistrationPage);
    }
  }

  doAlert(title: string, message: string) {
    let alert = this.alertCtrl.create({
        title: title,
        subTitle: message,
        buttons: ['OK']
    });
    alert.present();
  }
  
  ResendCode(){
    this.userRegistration.resendCode(this.navParams.get("email"), this);
  }

  cognitoCallback_resend_code(error: any, result: any){
    if (error != null) {
        this.doAlert("Resend", "Something went wrong...please try again");
    } else {
        this.doAlert("Success", "check your email address");
    }
  }
}