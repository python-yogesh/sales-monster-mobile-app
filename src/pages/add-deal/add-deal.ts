import { Component } from '@angular/core';
import { IonicPage, Events, NavController, NavParams, Nav, ToastController, LoadingController } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { SearchPage } from '../search/search';
import { CognitoUtil } from '../../providers/cognito.service'
import { UserLoginService } from '../../providers/userLogin.service';
import { DealService, ContactService, PipelineService } from '../../providers/api-services/user.service';
import { SharedComponent } from '../../providers/api-services/_sharedComponent';
import { ProductDetailPage } from '../product-detail/product-detail';



declare var $:any;

@IonicPage()
@Component({
  selector: 'page-add-deal',
  templateUrl: 'add-deal.html',
})
export class AddDealPage {
  private dealUUID;
  private deal_data = { contact_organization_uuid: {contact_organization_name:''}, contact_person_uuid: {person_name: ''},
    expected_close_date:'', deal_currency: 'USD', deal_title:''
  }

  private dealForm: FormGroup;
  private product_quantity;
  private _sharedComponent = new SharedComponent();
  org_icon: string = 'add';
  person_icon_new: string = 'add';

  constructor(public navCtrl: NavController, public events: Events, public navParams: NavParams, 
              public nav: Nav,
  						public deal_services: DealService, public formBuilder: FormBuilder, 
              public contact_services: ContactService, public toastCtrl: ToastController,
              public pipeline_service: PipelineService, public connito_utils: CognitoUtil,
              public loadingCtrl: LoadingController, public user_login_service: UserLoginService) {
  	this.get_currency();
    this.initialize_form();
    events.subscribe('contact:org', (userEventData) => {
      this.dealForm.controls['contact_organization_name'].setValue(userEventData.org_name);
      this.dealForm.controls['contact_organization_uuid'].setValue(userEventData.org_uuid);
      this.deal_data.deal_title = userEventData.org_name + ' deal';
      // this.org_name = userEventData;
      this.org_icon = "close";
    });
    events.subscribe('contact:person', (userEventData) => {
      this.dealForm.controls['contact_person_name'].setValue(userEventData.person_name);
      this.dealForm.controls['contact_person_uuid'].setValue(userEventData.person_uuid);
      this.deal_data.deal_title = userEventData.person_name + ' deal';
      this.person_icon_new = "close";
    });
    this.product_quantity = this.navParams.get('product_quantity');
    events.subscribe('attach_product',(stagesindex)=>{
      this.product_quantity;
      this.deal_data;
    });
  }

  private form_value;
  initialize_form(){
    this.dealForm = this.formBuilder.group({
      'contact_person_name':[''],
      'contact_organization_name':[''],
      'contact_person_uuid': [''],
      'contact_organization_uuid': [''],
      'deal_title': [''],
      'deal_value': ['', Validators.compose([Validators.pattern('^[+-]?([0-9]*[.])?[0-9]+$')])],
      'deal_currency': [''],
      'pipeline_uuid': [''],
      'user_username': [ this.user_login_service.cUtil.getCurrentUser().username ],
      'stage_uuid': [''],
      'expected_close_date': [''],
      'owner_follower': [true],
      'is_product': [false],
    });

    this.pipeline_list = this.navParams.get('list_of_pipeline');
    this.stages_list = this.navParams.get('list_of_stages');
    this.selected_pipeline = this.navParams.get('selected_pipeline_id');
    this.selected_stage_uuid = this.navParams.get('selected_stage_uuid');
    var data = this.navParams.get('single_deal_obj');
    if (data!=undefined) {
      this.deal_data = data;
      this.dealForm.controls['contact_person_uuid'].setValue(data.contact_person_uuid.uuid);
      this.dealForm.controls['contact_organization_uuid'].setValue(data.contact_organization_uuid.uuid);
      this.form_value = true;
      if(this.form_value == true){
        this.org_icon = 'close';
        this.person_icon_new = 'close';
      }
      /*console.log('value', this.deal_data);*/
      this.dealUUID = data.uuid;
    }else{
      this.deal_data.expected_close_date = this._sharedComponent.dateToYMD(new Date());
    }
  }

  private pipeline_list; 
  private selected_pipeline;
  private stages_list;
  private selected_stage_uuid;
  
  ionViewDidEnter(){
    // $('.ios .scroll-bar .scroll-content').css('margin-top', '100px');
    // $('.md .scroll-bar .scroll-content').css('margin-top', '100px');
  }

  ionViewDidLeave(){
    $('.ios .scroll-bar .scroll-content').css('margin-top', '100px');
    $('.md .scroll-bar .scroll-content').css('margin-top', '100px');
  }

  ionViewDidLoad() {
    /*console.log('ionViewDidLoad AddDealPage');*/
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
    });
    toast.present();
  }
 
  private loader;
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    this.loader.present();
  }

  // push on SearchPage when we click on input button
  push_on_person_search_page(input){
    this.navCtrl.push(SearchPage, {
      'value': 0
    });
  }

  // push on SearchPage when we click on input button
  contact_org_search_page(){
    this.navCtrl.push(SearchPage, {
      'value': 1
    });
  }

  organisation_icon(ioninput){
    if(ioninput.attributes[4].value == "add"){
      this.navCtrl.push(SearchPage, {
      'contact_org_list': this.navParams.get('contact_org_list'),
      'value': 1
    }); 
    }
    else{
       this.deal_data.contact_organization_uuid.contact_organization_name = "";
       this.org_icon = "add";

    }
  }

  push_on_product_list(){
    this.navCtrl.push(ProductDetailPage, {
      'deal_uuid':this.dealUUID,
    });
  }

  person_icon(ioninput){
    if(ioninput.attributes[4].value == "add"){
      this.navCtrl.push(SearchPage, {
      'contact_org_list': this.navParams.get('contact_org_list'),
      'value': 1
    }); 
    }
    else{
       this.deal_data.contact_person_uuid.person_name = "";
       this.person_icon_new = "add";
    }
  }


  private allCurrency;
  //Method for get all currency of a organization
  get_currency(){
    this.deal_services.get_organization_currency_field_api(true).subscribe(data=>{
        this.allCurrency = data['Items'];
    });
  }

  save_deal(){
    var data = this.dealForm.value;
    if (data.contact_person_uuid=='' || data.contact_organization_uuid=='') {
      alert('Please link a person and organization to your deal');
      return;
    }

    if(data.deal_value=='' || data.deal_value==undefined || data.deal_value==null){
      data.deal_value = 0;
    }

    if(data.deal_title=='' || data.deal_title==undefined || data.deal_title==null){
      data.deal_title = ' ';
    }
    delete data['contact_person_name'];
    delete data['contact_organization_name'];
    /*console.log(data);*/
    if (this.dealForm.valid) {
      this.presentLoading();
      this.deal_services.add_deal(this.selected_pipeline, data).subscribe(data=>{
        this.loader.dismiss();
        if (data.ResponseMetadata.HTTPStatusCode == 200) {
          this.events.publish('person_list:listing');
          this.events.publish('organisation_detail:listing');
          this.presentToast('Deal added successfully');
          this.events.publish('pipeline:stage', this.selected_pipeline);
          this.navCtrl.pop();
        }
      }, (err)=>{
        this.loader.dismiss();
      });
    }
  }

  update_deal(){
    var data = this.dealForm.value;
    if (data.contact_person_uuid=='' || data.contact_organization_uuid=='') {
      alert('Please link a person and organization to your deal');
      return;
    }

    if(data.deal_value=='' || data.deal_value==undefined || data.deal_value==null){
      data.deal_value = 0;
    }

    if(data.deal_title=='' || data.deal_title==undefined || data.deal_title==null){
      data.deal_title = ' ';
    }
    delete this.dealForm.value['contact_person_name'];
    delete this.dealForm.value['contact_organization_name'];
    /*console.log(data);*/
    this.presentLoading();
    if (this.form_value) { // update deal
      data.deal_currency = this.deal_data.deal_currency;
      this.deal_services.update_deal_api(this.dealUUID, data).subscribe(data=>{
        if (data.ResponseMetadata.HTTPStatusCode == 200) {
            this.loader.dismiss();
            this.events.publish('person_list:listing');
            this.events.publish('organisation_detail:listing');
            this.presentToast('Deal update successfully');
            this.events.publish('get_deal',this.stagesindex);
            // this.navCtrl.pop();
          }
        }, (err)=>{
        this.loader.dismiss();
      });
    }
  }
  
  private stagesindex;
  onSelectChange(selectedValue: any) {
    /*console.log('Selected', selectedValue);*/
    this.stages_list.filter((x, indx)=>{ 
      if (x.uuid == selectedValue) {
        this.stagesindex = indx;
        
      }
    });
  }
  deleteDeal(){
      var confirmBox = confirm("Are you sure you want to delete this deal!"); 
      if (confirmBox == true) { 
        let data = {};
        this.deal_services.delete_deal_api(this.dealUUID, data).subscribe(data=>{
          if(!data['error']){
            alert('Delete Deal successfully');
            this.events.publish('get_deal_list');
            this.navCtrl.pop();
          }
        });
      }
    }
}